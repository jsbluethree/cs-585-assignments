// ActorInfo.cpp
// Chris Bowers 11/10/2014

#include "ActorInfo.h"

void ActorInfo::GetActorInfo(std::string filepath){
	JsonObject* actorJson = static_cast<JsonObject*>(JsonParser::GetInstance().Parse(filepath));
	JsonObject* statJson = static_cast<JsonObject*>(actorJson->trie["stats"]);	
	JsonObject* traitJson = static_cast<JsonObject*>(actorJson->trie["traits"]);
	
	for (unsigned int i = 0; i < statJson->keyList.Length(); i++){
		stats[statJson->keyList[i]] = static_cast<JsonNumber*>(statJson->trie[statJson->keyList[i]])->number;
	}
	
	for (unsigned int i = 0; i < traitJson->keyList.Length(); i++){
		traits[traitJson->keyList[i]] = static_cast<JsonString*>(traitJson->trie[traitJson->keyList[i]])->string;
	}
	
	delete actorJson;
}