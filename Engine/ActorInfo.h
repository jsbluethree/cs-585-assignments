// ActorInfo.h
// Chris Bowers 11/10/2014

#ifndef __ACTORINFO_H__
#define __ACTORINFO_H__

#include "JsonParser.h"
#include <map>

class ActorInfo{
public:
	std::map<std::string, float> stats;
	std::map<std::string, std::string> traits;
	
	void GetActorInfo(std::string filepath);
}

#endif
