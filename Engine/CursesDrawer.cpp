// CursesDrawer.cpp
// Chris Bowers 11/8/2014

#include "CursesDrawer.h"

CursesDrawer::CursesDrawer() : cameraOffsetX(0), cameraOffsetY(0), cameraLeft(false), cameraRight(false), cameraUp(false), cameraDown(false), screen(nullptr) {
	keyDownListener.drawer = this;
	keyUpListener.drawer = this;
	Input::GetInstance().AddListener("keydown", &keyDownListener);
	Input::GetInstance().AddListener("keyup", &keyUpListener);
	
}

void CursesDrawer::GetIconInfo(std::string filepath){
	JsonObject* iconJson = static_cast<JsonObject*>(JsonParser::GetInstance().Parse(filepath));
	for (unsigned int i = 0; i < iconJson->keyList.Length(); i++){
		if (can_change_color()){
			init_color(i + 1,	static_cast<JsonNumber*>(static_cast<JsonArray*>(static_cast<JsonObject*>(iconJson->trie[iconJson->keyList[i]])->trie["color"])->array[0])->number,
								static_cast<JsonNumber*>(static_cast<JsonArray*>(static_cast<JsonObject*>(iconJson->trie[iconJson->keyList[i]])->trie["color"])->array[1])->number,
								static_cast<JsonNumber*>(static_cast<JsonArray*>(static_cast<JsonObject*>(iconJson->trie[iconJson->keyList[i]])->trie["color"])->array[2])->number);
			init_pair(i + 1, i + 1, COLOR_BLACK);
			icons[iconJson->keyList[i]] = static_cast<JsonString*>(static_cast<JsonObject*>(iconJson->trie[iconJson->keyList[i]])->trie["char"])->string[0] | COLOR_PAIR(i + 1);
		}
		else
			icons[iconJson->keyList[i]] = static_cast<JsonString*>(static_cast<JsonObject*>(iconJson->trie[iconJson->keyList[i]])->trie["char"])->string[0] | COLOR_PAIR(0);
	}
	
	delete iconJson;
}

void CursesDrawer::AddDrawable(IDrawable* drawable) { drawables[drawable->ZValue()].first.Append(drawable); }

void CursesDrawer::RemoveDrawable(IDrawable* drawable){
	for (unsigned int i = 0; i < drawables[drawable->ZValue()].first.Length(); i++){
		if (drawables[drawable->ZValue()].first[i] == drawable){
			drawables[drawable->ZValue()].first.Delete(i);
			return;
		}
	}
}

void CursesDrawer::AddBigDrawable(IBigDrawable* bigDrawable) { drawables[bigDrawable->ZValue()].second.Append(bigDrawable); }

void CursesDrawer::RemoveBigDrawable(IBigDrawable* bigDrawable){
	for (unsigned int i = 0; i < drawables[bigDrawable->ZValue()].second.Length(); i++){
		if (drawables[bigDrawable->ZValue()].second[i] == bigDrawable){
			drawables[bigDrawable->ZValue()].second.Delete(i);
			return;
		}
	}
}

void CursesDrawer::Tick(float dt){
	int maxY;
	int maxX;
	getmaxyx(screen, maxY, maxX);
	if (cameraUp && cameraOffsetY > 0)
		cameraOffsetY--;
	if (cameraDown && cameraOffsetY < maxY - (LINES - 4))
		cameraOffsetY++;
	if (cameraLeft && cameraOffsetX > 0)
		cameraOffsetX--;
	if (cameraRight && cameraOffsetX < maxX - COLS)
		cameraOffsetX++;

	for (auto it = drawables.begin(); it != drawables.end(); ++it){
		for (unsigned int i = 0; i < it.second.first.Length(); i++){
			mvwaddch(screen, it.second.first[i]->YPos(), it.second.first[i]->XPos(), icons[it.second.first[i]->Icon()]);
		}
		for (unsigned int i = 0; i < it.second.second.Length(); i++){
			for (unsigned int xOffset = 0; xOffset < it.second.second[i]->Width(); xOffset++){
				for (unsigned int yOffset = 0; yOffset < it.second.second[i]->Height(); yOffset++){
					mvwaddch(screen, it.second.second[i]->YPos() + yOffset, it.second.second[i]->XPos() + xOffset, icons[it.second.second[i]->Icons()[xOffset + yOffset * it.second.second[i]->Width()]]);
				}
			}
		}
	}
}

void CursesDrawer::OnKeyDown::Execute(IEvent* event){
	switch (static_cast<KeyDownEvent*>(event)->key){
	case 'w':
		cameraUp = true;
		break;
	case 'a':
		cameraLeft = true;
		break;
	case 's':
		cameraDown = true;
		break;
	case 'd':
		cameraRight = true;
		break;
	}
}

void CursesDrawer::OnKeyUp::Execute(IEvent* event){
	switch (static_cast<KeyUpEvent*>(event)->key){
	case 'w':
		cameraUp = false;
		break;
	case 'a':
		cameraLeft = false;
		break;
	case 's':
		cameraDown = false;
		break;
	case 'd':
		cameraRight = false;
		break;
	}
}
