// CursesDrawer.h
// Chris Bowers 11/7/2014

#ifndef __CURSESDRAWER_H__
#define __CURSESDRAWER_H__

#include <ncurses.h>
#include <map>
#include "DynamicArray.h"
#include "IDrawable.h"
#include "IBigDrawable.h"
#include "ITickable.h"
#include "JsonParser.h"
#include "Input.h"
#include "ICallback.h"

class CursesDrawer : public ITickable{
public:
	int cameraOffsetX;
	int cameraOffsetY;
	bool cameraLeft;
	bool cameraRight;
	bool cameraUp;
	bool cameraDown;

	WINDOW* screen;	// pointer to the window to be drawn to
	
	std::map<std::string, chtype> icons;	// map of names of icons to chtypes representing them
	
	std::map<int, std::pair<DynamicArray<IDrawable*>, DynamicArray<IBigDrawable*>>> drawables;	// drawables mapped by zvalue
	
	class OnKeyDown : public ICallback{
	public:
		CursesDrawer* drawer;
		void Execute(IEvent* event);
	} keyDownListener;
	
	class OnKeyUp : public ICallback{
		CursesDrawer* drawer;
		void Execute(IEvent* event);
	} keyUpListener;
	
	
	CursesDrawer();
	
	void GetIconInfo(std::string filepath);		// populates icons map from json
	
	void AddDrawable(IDrawable* drawable);		// adds a drawable
	void RemoveDrawable(IDrawable* drawable);	// removes a drawable
	
	void AddBigDrawable(IBigDrawable* bigDrawable);		// adds a big drawable
	void RemoveBigDrawable(IBigDrawable* bigDrawable);	// removes a big drawable
	
	void Tick(float dt);	// draws all drawables and big drawables
}

#endif
