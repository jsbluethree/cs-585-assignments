// DamageInfo.cpp
// Chris Bowers 11/11/2014

#include "DamageInfo.h"

DamageInfo::DamageInfo(float damage, std::string type) : damage(damage), type(type) {}