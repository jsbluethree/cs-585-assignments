// DamageInfo.h
// Chris Bowers 11/11/2014

#ifndef __DAMAGEINFO_H__
#define __DAMAGEINFO_H__

#include <string>

class DamageInfo{
public:
	float damage;
	std::string type;

	DamageInfo(float damage, std::string type);
};

#endif
