// Dispatcher.cpp
// Chris Bowers 9/23/2014

#include "Dispatcher.h"

void Dispatcher::AddListener(std::string eventType, ICallback* callback){ deferAddList.Append(std::make_pair(eventType, callback)); }

void Dispatcher::RemoveListener(std::string eventType, ICallback* callback){
	if (listenerTrie.Search(eventType)){
		deferRemoveList.Append(std::make_pair(eventType, callback));
	}
}

void Dispatcher::AddEvent(IEvent* event){ eventList.Append(event); }

void Dispatcher::Dispatch(IEvent* event){
	DynamicArray<ICallback*>* listeners = &listenerTrie[event->Type()];
	for (unsigned int i = 0; i < listeners->Length(); i++){
		listeners->At(i)->Execute(event);
	}
	delete event;
}

void Dispatcher::Tick(float dt){
	for (unsigned int i = 0; i < eventList.Length(); i++){
		Dispatch(eventList[i]);
	}
	eventList.Initialize();
	
	for (unsigned int i = 0; i < deferAddList.Length(); i++){
		listenerTrie[deferAddList[i].first].Append(deferAddList[i].second);
	}

	for (unsigned int i = 0; i < deferRemoveList.Length(); i++){
		for (unsigned int j = 0; j < listenerTrie[deferRemoveList[i].first].Length(); j++){
			if (listenerTrie[deferRemoveList[i].first][j] == deferRemoveList[i].second)
				listenerTrie[deferRemoveList[i].first].Delete(j);
		}
	}

	deferAddList.Initialize();
	deferRemoveList.Initialize();
}

Dispatcher::~Dispatcher(){
	for (unsigned int i = 0; i < eventList.Length(); i++)
		delete eventList[i];
}	