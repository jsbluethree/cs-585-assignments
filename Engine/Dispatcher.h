// Dispatcher.h
// Chris Bowers 9/23/2014

#ifndef __DISPATCHER_H__
#define __DISPATCHER_H__

#include "DynamicArray.h"
#include "ICallback.h"
#include "IEvent.h"
#include "ITickable.h"
#include "Trie.h"
#include <string>
#include <utility>

class Dispatcher : public ITickable{
private:
	Trie<DynamicArray<ICallback*>> listenerTrie;	// trie of listeners stored by event type
	DynamicArray<IEvent*> eventList;				// list of events to be checked in Tick()
	
	DynamicArray<std::pair<std::string, ICallback*>> deferAddList;		// list of deferred listeners to add
	DynamicArray<std::pair<std::string, ICallback*>> deferRemoveList;	// list of deferred listeners to remove
	

public:

	void AddListener(std::string eventType, ICallback* callback);	// registers a listener for a particular event type
	void RemoveListener(std::string eventType, ICallback* callback);						// removes a listener

	void AddEvent(IEvent* event);	// adds an event to the list
	void Dispatch(IEvent* event);	// passes the event to the appropriate listener, then deletes it
	void Tick(float dt);			// dispatches all events in eventList, then adds/removes new listeners
	
	~Dispatcher();
};

#endif
