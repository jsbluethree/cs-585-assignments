// GenericActor.cpp
// Chris Bowers 11/7/2014

#include "GenericActor.h"

GenericActor::GenericActor(std::map<std::string, float> initialStats, std::map<std::string, std::string> initialTraits) : stats(initialStats), traits(initialTraits) {}

void DoDamage(DamageInfo& damageInfo){
	if (stats.count("health") > 0){
		if (stats.count(damageInfo.type + "resist") > 0){
			stats["health"] -= (damageInfo.damage -= stats[damageInfo.type + "resist"]);
		}
		else
			stats["health"] -= damageInfo.damage;
	}
}

std::string GenericActor::Icon() { return traits["icon"]; }

int GenericActor::ZValue() { return (int)stats["zvalue"]; }

int GenericActor::XPos() { return GetX(); }

int GenericActor::YPos() { return GetY(); }