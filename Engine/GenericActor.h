// GenericActor.h
// Chris Bowers 11/7/2014

#ifndef __GENERICACTOR_H__
#define __GENERICACTOR_H__

#include "DamageInfo.h"
#include "SceneNode.h"
#include <map>
#include <string>

class GenericActor : public SceneNode, public IDrawable{
public:
	std::map<std::string, float> stats;
	std::map<std::string, std::string> traits;
	
	GenericActor(std::map<std::string, float> initialStats, std::map<std::string, std::string> initialTraits);
	
	void DoDamage(DamageInfo& damageInfo);
	
	std::string Icon();	
	int ZValue();
	int XPos();
	int YPos();
};

#endif
