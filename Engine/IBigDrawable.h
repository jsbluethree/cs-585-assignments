// IBigDrawable.h
// Chris Bowers 11/7/2014

#ifndef __IBIGDRAWABLE_H__
#define __IBIGDRAWABLE_H__

#include "DynamicArray.h"
#include <string>

class IBigDrawable{
public:
	virtual unsigned int Height() = 0;
	virtual unsigned int Width() = 0;
	virtual DynamicArray<std::string>& Icons() = 0;
	virtual int ZValue() = 0;
	virtual int XPos() = 0;
	virtual int YPos() = 0;
	
	virtual ~IBigDrawable() {}
};

#endif
