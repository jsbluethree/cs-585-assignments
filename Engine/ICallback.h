// ICallback.h 
// Chris Bowers 9/23/2014

#ifndef __ICALLBACK_H__
#define __ICALLBACK_H__

#include "IEvent.h"

class ICallback{
public:
	virtual void Execute(IEvent* event) = 0;	// virtual function does something when triggered by an event
};

#endif
