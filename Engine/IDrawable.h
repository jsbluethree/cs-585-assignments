// IDrawable.h
// Chris Bowers 11/7/2014

#ifndef __IDRAWABLE_H__
#define __IDRAWABLE_H__

#include <string>

class IDrawable.h{
public:
	virtual std::string Icon() = 0;
	virtual int ZValue() = 0;
	virtual int XPos() = 0;
	virtual int YPos() = 0;
	virtual ~IDrawable() {}
};

#endif
