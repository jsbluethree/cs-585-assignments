// IEvent.h 
// Chris Bowers 9/23/2014

#ifndef __IEVENT_H__
#define __IEVENT_H__

#include <string>

class IEvent{
public:
	std::string Type() = 0;	// returns type
	~IEvent() {}
};

#endif
