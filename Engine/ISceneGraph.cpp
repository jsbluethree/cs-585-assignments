// ISceneGraph.cpp
// Chris Bowers 9/10/2014

#include "ISceneGraph.h"

void ISceneGraph::AddNode(SceneNode* node){
	std::string hash = Map(node->GetX(), node->GetY());
	// adds a new array if there is no array at that position, then puts the node in that array

	Debug::GetInstance()->VLog("Adding node " + node->name + " at position " + hash + " to SceneGraph");
	nodeTrie[hash].Append(node);
}

void ISceneGraph::RemoveNode(SceneNode* node){
	std::string hash = Map(node->GetX(), node->GetY());
	for (unsigned int i = 0; i < nodeTrie[hash].Length(); ++i){
		if (nodeTrie[hash][i] == node)
			nodeTrie[hash].Delete(i);
	}
}

void ISceneGraph::UpdateNode(SceneNode* node, int x, int y){
	RemoveNode(node);
	node->SetX(x);
	node->SetY(y);
	AddNode(node);
}

DynamicArray<SceneNode*> ISceneGraph::GetColliders(SceneNode* node){
	std::string hash = Map(node->GetX(), node->GetY());
	return nodeTrie[hash];
}

DynamicArray<SceneNode*> ISceneGraph::GetColliders(int x, int y){
	std::string hash = Map(x, y);
	return nodeTrie[hash];
}

DynamicArray<SceneNode*> ISceneGraph::GetColliders(int x, int y, int radius){
	DynamicArray<SceneNode*> output;
	for (int i = x - radius; i <= x + radius; i++){
		for (int j = y - radius; j <= y + radius; j++){
			std::string hash = Map(i, j);
			if (nodeTrie.Search(hash)){
				output.Append(nodeTrie[hash]);
			}
		}
	}
	return output;
}

DynamicArray<SceneNode*> ISceneGraph::GetColliders(int cornerx1, int cornery1, int cornerx2, int cornery2){
	DynamicArray<SceneNode*> output;
	for (int i = cornerx1; i <= cornerx2; i++){
		for (int j = cornery1; j <= cornery2; j++){
			std::string hash = Map(i, j);
			if (nodeTrie.Search(hash)){
				output.Append(nodeTrie[hash]);
			}
		}
	}
	return output;
}