// ISceneGraph.h
// Chris Bowers 9/10/2014

#ifndef __ISCENEGRAPH_H__
#define __ISCENEGRAPH_H__

#include "Debug.h"
#include "SceneNode.h"
#include "DynamicArray.h"
#include "Trie.h"

class ISceneGraph{
public:
	Trie<DynamicArray<SceneNode*>> nodeTrie;	// trie represents game space

	virtual std::string Map(int x, int y) = 0;				// maps a coordinate point to a base-26 integer represented as a string
	void AddNode(SceneNode* node);							// adds a node
	void RemoveNode(SceneNode* node);						// removes a node
	void UpdateNode(SceneNode* node, int x, int y);			// moves a node
	DynamicArray<SceneNode*> GetColliders(SceneNode* node);	// returns a list of nodes at the given position
	DynamicArray<SceneNode*> GetColliders(int x, int y);
	DynamicArray<SceneNode*> GetColliders(int x, int y, int radius);
	DynamicArray<SceneNode*> GetColliders(int cornerx1, int cornery1, int cornerx2, int cornery2);
};

#endif
