// IState.h
// Chris Bowers 10/4/2014

#ifndef __ISTATE_H__
#define __ISTATE_H__

#include "Dispatcher.h"
#include "DynamicArray.h"
#include "SceneNode.h"
#include <map>

class IState{
public:
	Dispatcher events;	// dispatcher for sending out state change or other events	
	
	virtual void Enter() {}	// function called when a state is entered
	virtual void Run() = 0;		// function for performing state-specific logic
	virtual void Exit() {}	// function called when a state is exited

	virtual ~IState() {}
};

#endif
