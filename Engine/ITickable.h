// ITickable.h 
// Chris Bowers 9/10/2014

#ifndef __ITICKABLE_H__
#define __ITICKABLE_H__

class ITickable{
public:
	virtual void Tick(float dt) = 0; // virtual function used to update objects
	
	virtual ~ITickable(){};	// virtual destructor
};

#endif
