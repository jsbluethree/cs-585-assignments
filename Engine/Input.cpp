// Input.cpp
// Chris Bowers 11/6/2014

#include "Input.h"

Input* Input::instance = nullptr;

Input::Input() {}

Input::~Input() {}

Input& Input::GetInstance(){
	if (!instance)
		instance = new Input;
	return *instance;
}

void Input::Tick(float dt){
	// set all elements in downKeys to false
	for (std::map<int, bool>::iterator it = downKeys.begin(); it != downKeys.end(); ++it){
		it->second = false;
	}
	
	// check each key in input queue
	int ch;
	while ((ch = getch()) != ERR){
		// if it was not already in downKeys, send a KeyDownEvent
		if (downKeys.count(ch) == 0){
			events.AddEvent(new KeyDownEvent(ch));
		}
		// set that element to true
		downKeys[ch] = true;
	}
	
	// check if any downKeys are still false, and if so add them to upKeys and send a KeyUpEvent
	for (std::map<int, bool>::iterator it = downKeys.begin(); it != downKeys.end(); ++it){
		if (it->second == false){
			upKeys.Append(it->first);
			events.AddEvent(new KeyUpEvent(it->first));
		}
	}
	
	// remove all keys in upKeys from downKeys and clear upKeys
	for (unsigned int i = 0; i < upKeys.Length() ; i++){
		downKeys.erase(upKeys[i]);
	}
	upKeys.Initialize();
}

KeyUpEvent::KeyUpEvent(int key) : key(key) { type = "keyup"; }

KeyDownEvent::KeyDownEvent(int key) : key(key) { type = "keydown"; }
