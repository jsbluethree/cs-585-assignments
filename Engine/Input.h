// Input.h
// Chris Bowers 11/6/2014

#ifndef __INPUT_H__
#define __INPUT_H__

#include <ncurses.h>
#include <map>
#include "Dispatcher.h"
#include "DynamicArray.h"
#include "IEvent.h"
#include "ITickable.h"

class Input : public ITickable{
private:
	std::map<int, bool> downKeys;
	DynamicArray<int> upKeys;
	
	static Input* instance;
	Input();
	
public:
	Dispatcher events;
	
	~Input();
	static Input& GetInstance();	
	void Tick(float dt);
};

class KeyUpEvent : public IEvent{
public:
	int key;
	
	KeyUpEvent(int key);
};

class KeyDownEvent : public IEvent{
public:
	int key;
	
	KeyDownEvent(int key);
};

#endif
