// LevelMap.cpp
// Chris Bowers 11/9/2014

#include "LevelMap.h"

void LevelMap::GetLevelInfo(std::string filepath){
	JsonObject* levelJson = static_cast<JsonObject*>(JsonParser::GetInstance().Parse(filepath));
	
	height = static_cast<JsonNumber*>(levelJson->trie["height"])->number;
	width = static_cast<JsonNumber*>(levelJson->trie["width"])->number;
	
	for (unsigned int i = 0; i < width * height; i++){
		tiles.Append(static_cast<JsonString*>(levelJson->trie["defaulttile"])->string);
	}
	
	// add in other tiles if necessary
	
	delete levelJson;
}

unsigned int LevelMap::Height() { return height; }

unsigned int LevelMap::Width() { return width; }

DynamicArray<std::string>& LevelMap::Icons() { return tiles; }

int LevelMap::ZValue() { return 0; }

int LevelMap::XPos() { return 0; }

int LevelMap::YPos() { return 0; }
