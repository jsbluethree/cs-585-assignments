// LevelMap.h
// Chris Bowers 11/9/2014

#ifndef __LEVELMAP_H__
#define __LEVELMAP_H__

#include <string>
#include "IBigDrawable.h"
#include "JsonParser.h"

class LevelMap : public IBigDrawable{
private:
	DynamicArray<std::string> tiles;
	unsigned int height;
	unsigned int width;
	
public:	
	void GetLevelInfo(std::string filepath);
	
	unsigned int Height();
	unsigned int Width();
	DynamicArray<std::string>& Icons();
	int ZValue();
	int XPos();
	int YPos();
};

#endif
