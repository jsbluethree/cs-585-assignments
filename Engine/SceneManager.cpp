// SceneManager.cpp 
// Chris Bowers 9/10/2014

#include "SceneManager.h"

SceneManager* SceneManager::instance = nullptr;

SceneManager::SceneManager(){}

SceneManager& SceneManager::GetInstance(){
	if (!instance)
		instance = new SceneManager();
	return *instance;
}

void SceneManager::Tick(float dt){
	for (unsigned int i = 0; i < tickList.Length(); ++i)
		tickList[i]->Tick(dt);
}

void SceneManager::AddTickable(ITickable* tickable){ tickList.Append(tickable); }

void SceneManager::RemoveTickable(ITickable* tickable){
	for (unsigned int i = 0; i < tickList.Length(); ++i){
		if (tickable == tickList[i]){
			tickList.Delete(i);
			return;
		}
	}
}

void SceneManager::AddSceneNode(SceneNode* node){ nodeList.Append(node); }

void SceneManager::RemoveSceneNode(SceneNode* node){
	for (unsigned int i = 0; i < nodeList.Length(); ++i){
		if (node == nodeList[i]){
			nodeList.Delete(i);
			return;
		}
	}
}

DynamicArray<SceneNode*> SceneManager::NodeList(){ return nodeList; }