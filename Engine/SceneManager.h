// SceneManager.h 
// Chris Bowers 9/10/2014

#ifndef __SCENEMANAGER_H__
#define __SCENEMANAGER_H__

#include "Debug.h"
#include "DynamicArray.h"
#include "ITickable.h"
#include "SceneNode.h"

class SceneManager : public ITickable{
private:
	static SceneManager* instance;		// pointer to itself
	DynamicArray<SceneNode*> nodeList;	// list of all scenenodes
	DynamicArray<ITickable*> tickList;	// list of all tickables
	SceneManager();						// constructor
public:
	static SceneManager& GetInstance();		// creates an instance if needed, returns pointer to self
	void Tick(float dt);					// ticks all tickables
	void AddTickable(ITickable* tickable);
	void RemoveTickable(ITickable* tickable);
	void AddSceneNode(SceneNode* node);
	void RemoveSceneNode(SceneNode* node);
	
	DynamicArray<SceneNode*> NodeList();

};

#endif
