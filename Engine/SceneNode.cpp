// SceneNode.cpp 
// Chris Bowers 9/10/2014

#include "SceneNode.h"

int SceneNode::GetX() { return xpos; }
int SceneNode::GetY() { return ypos; }

void SceneNode::SetX(int x) { xpos = x; }
void SceneNode::SetY(int y) { ypos = y; }

SceneNode::~SceneNode() {}