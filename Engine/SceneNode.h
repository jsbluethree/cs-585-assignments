// SceneNode.h 
// Chris Bowers 9/10/2014

#ifndef __SCENENODE_H__
#define __SCENENODE_H__

#include <string>

class SceneNode{
private:
	int xpos;	// position
	int ypos;
public:
	std::string name;
	int GetX();
	int GetY();
	void SetX(int x);
	void SetY(int y);
	
	virtual ~SceneNode();
};

#endif
