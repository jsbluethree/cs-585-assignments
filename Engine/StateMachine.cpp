// StateMachine.cpp
// Chris Bowers 10/1/2014

#include "StateMachine.h"

StateEvent::StateEvent(std::string nextState) : nextState(nextState) {}

std::string StateEvent::Type() { return "state"; };

StateMachine::StateMachine(){
	stateTransitionListener.machine = this;
}

void StateMachine::OnStateTransition::Execute(IEvent* event){
	machine->currentState->Exit();
	machine->currentState->events.RemoveListener("state", this);
	machine->currentState = machine->stateLookup[static_cast<StateEvent*>(event)->nextState];
	machine->currentState->events.AddListener("state", this);
	machine->currentState->Enter();
	Debug::GetInstance().Log(D_STATEMACHINE, "State Machine transition");
}
