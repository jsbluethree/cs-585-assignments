// StateMachine.h
// Chris Bowers 10/1/2014

#ifndef __STATEMACHINE_H__
#define __STATEMACHINE_H__

#include "ICallback.h"
#include "IEvent.h"
#include "ITickable.h"
#include "IState.h"
#include <map>
#include <string>

class StateEvent : public IEvent{
public:
	std::string nextState;	// name of the next state to move to

	StateEvent(std::string nextState);	// constructor
};

class StateMachine{
public:
	IState* currentState;							// pointer to current state
	std::map<std::string, IState*> stateLookup;		// map of generic state names to specific states

	class OnStateTransition : public ICallback{
	public:
		StateMachine* machine;
		void Execute(IEvent* event);
	} stateTransitionListener;						// listener for transition events

	// constructor
	StateMachine();
};

#endif
