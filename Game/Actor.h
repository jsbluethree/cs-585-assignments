// Actor.h
// Chris Bowers 9/10/2014

#ifndef __ACTOR_H__
#define __ACTOR_H__

#include "SceneNode.h"
#include <string>

class Actor: public SceneNode{
public:
	Actor(std::string label = "", int x = 0, int y = 0);	// constructor
};

#endif
