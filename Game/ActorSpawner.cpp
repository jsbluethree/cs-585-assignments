// ActorSpawner.cpp
// Chris Bowers 9/13/2014

#include "ActorSpawner.h"

ActorSpawner::ActorSpawner(std::string filename, FixedGrid* pgrid, RandomController* pcontroller, SceneManager* pmanager){
	grid = pgrid;
	controller = pcontroller;
	pmanager->AddTickable(this);
	std::ifstream infile;
	infile.open(filename);
	while (!infile.eof()){
		std::string name;
		std::getline(infile, name);
		nameList.Append(name);
	}
	infile.close();
	spawned = 0;
}


void ActorSpawner::Tick(float dt){
	if (dt >= 2 * spawned){
		int randname = rand() % nameList.Length();
		std::string name = nameList[randname];
		controller->NewActor(name);
		spawned++;
	}
}
