// ActorSpawner.h
// Chris Bowers 9/13/2014

#ifndef __ACTORSPAWNER_H__
#define __ACTORSPAWNER_H__

#include "Actor.h"
#include "ITickable.h"
#include "DynamicArray.h"
#include "RandomController.h"
#include "FixedGrid.h"
#include "SceneManager.h"
#include <fstream>

class ActorSpawner : public ITickable{
private:
	DynamicArray<std::string> nameList;	// list of names to be assigned to actors
	FixedGrid* grid;					// pointer to the grid to add actors to
	RandomController* controller;		// pointer to the controller
	int spawned;						// # of actors spawned
public:
	ActorSpawner(std::string filename, FixedGrid* pgrid, RandomController* pcontroller, SceneManager* pmanager);	// constructor
	void Tick(float dt);																							// spawns actors and adds them to proper lists
};

#endif
