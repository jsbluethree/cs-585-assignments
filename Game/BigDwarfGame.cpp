// BigDwarfGame.cpp
// Chris Bowers

#include "BigDwarfGame.h"

void BigDwarfGame::InitCurses(){
	initscr();
	cbreak();
	noecho();
	keypad(stdscr, TRUE);
	nodelay(stdscr, TRUE);
	start_color();
}

void BigDwarfGame::InitInfo(){
	Info::GetInstance().actors["dwarf"].GetActorInfo("Game/DwarfGame/dwarf.json");
	Info::GetInstance().actors["orc"].GetActorInfo("Game/DwarfGame/orc.json");
	Info::GetInstance().actors["cursor"].GetActorInfo("Game/DwarfGame/cursor.json");
	Info::GetInstance().buildings["greathall"].GetActorInfo("Game/DwarfGame/greathall.json");
	Info::GetInstance().buildings["apothecary"].GetActorInfo("Game/DwarfGame/apothecary.json");
	Info::GetInstance().buildings["blacksmith"].GetActorInfo("Game/DwarfGame/blacksmith.json");
	levelDrawer.GetIconInfo("Game/DwarfGame/icon.json");
	levelMap.GetLevelInfo("Game/DwarfGame/level.json");
}

void BigDwarfGame::InitLevel(){
	keyDownListener.game = this;
	Input::GetInstance().AddListener("keydown", &keyDownListener);
	DwarfSceneManager::GetInstance().AddTickable(&levelDrawer);
	DwarfSceneManager::GetInstance().AddTickable(&Input::GetInstance());
	levelDrawer.screen = newpad(levelMap.Height(), levelMap.Width());
	levelDrawer.AddDrawable(&levelMap);
	DwarfSceneManager::GetInstance().sceneGraph.SetHeight(levelMap.Height());
	DwarfSceneManager::GetInstance().sceneGraph.SetWidth(levelMap.Width());
	controllers.drawer = &levelDrawer;
	controllers.AddInitActors("Game/DwarfGame/initactors.json");
	textUI.textWindow = newwin(4, COLS, LINES - 4, 0);
	textUI.player = controllers.player;
	
	// more
}

void BigDwarfGame::Run(){
	running = true;
	
	auto startClock = std::chrono::high_resolution_clock::now();
	
	while (running){
		DwarfSceneManager::GetInstance().Tick(std::chrono::duration<float>(std::chrono::high_resolution_clock::now() - startClock).count());
		pnoutrefresh(levelDrawer.screen, levelDrawer.cameraOffsetY, LevelDrawer.cameraOffsetX, 0, 0, LINES - 5, COLS - 1);
		doupdate();
	}
}

void BigDwarfGame::Exit(){
	endwin();
}

void BigDwarfGame::OnKeyDown::Execute(IEvent* event){
	if (static_cast<KeyDownEvent*>(event)->key == 27)
		game->running = false;
}