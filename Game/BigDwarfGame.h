// BigDwarfGame.h
// ChrisBowers

#ifndef __BIGDWARFGAME_H__
#define __BIGDWARFGAME_H__

#include "ControllerFactory.h"
#include "CursesDrawer.h"
#include "DwarfSceneManager.h"
#include "DwarfUI.h"
#include "JsonParser.h"
#include "LevelMap.h"
#include "Info.h"
#include "Input.h"
#include <string>
#include <ncurses.h>
#include <chrono>

class BigDwarfGame{
public:
	bool running;
	
	ControllerFactory controllers;
	LevelMap levelMap;
	CursesDrawer levelDrawer;
	DwarfUI textUI;
	
	class OnKeyDown : public ICallback{
	public:
		BigDwarfGame* game;
		void Execute(IEvent* event);
	} keyDownListener;
	
	void InitCurses();
	void InitInfo();	// initializes info	
	void InitLevel();	// initializes level and gives the drawer a window	
	void Run(); 		// main game loop
	void Exit();		// performs cleanup
	
};

#endif
