// BuildingActor.cpp
// Chris Bowers

#include "BuildingActor.h"

BuildingActor::BuildingActor(std::map<std::string, float> initialStats, std::map<std::string, std::string> initialTraits, DynamicArray<std::string icons) : stats(initialStats), traits(initialTraits), icons(icons) {}

unsigned int BuildingActor::Height() { return (unsigned int)stats["height"]; }

unsigned int BuildingActor::Width() { return (unsigned int)stats["width"]; }

DynamicArray<std::string>& BuildingActor::Icons() { return icons; }

int BuildingActor::ZValue() { return (int)stats["zvalue"]; }

int BuildingActor::XPos() { return GetX(); }

int BuildingActor::YPos() { return GetY(); }