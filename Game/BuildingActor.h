// BuildingActor.h
// Chris Bowers

#ifndef __BUILDINGACTOR_H__
#define __BUILDINGACTOR_H__

#include "IBigDrawable.h"
#include "SceneNode.h"
#include <map>
#include <string>
#include "DynamicArray.h"

class BuildingActor : public SceneNode, public IBigDrawable{
public:
	std::map<std::string, float> stats;
	std::map<std::string, std::string> traits;
	DynamicArray<std::string> icons;
	
	BuildingActor(std::map<std::string, float> initialStats, std::map<std::string, std::string> initialTraits, DynamicArray<std::string> icons);
	
	unsigned int Height();
	unsigned int Width();
	DynamicArray<std::string>& Icons();
	int ZValue();
	int XPos();
	int YPos();
};

#endif
