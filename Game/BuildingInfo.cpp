// BuildingInfo.cpp
// Chris Bowers

#include "BuildingInfo.h"

void BuildingInfo::GetBuildingInfo(std::string filepath){
	JsonObject* buildingJson = static_cast<JsonObject*>(JsonParser::GetInstance().Parse(filepath));
	JsonObject* statJson = static_cast<JsonObject*>(buildingJson->trie["stats"]);
	JsonObject* traitJson = static_cast<JsonObject*>(buildingJson->trie["traits"]);
	JsonArray* iconJson = static_cast<JsonArray*>(buildingJson->trie["icons"]);
	
	for (unsigned int i = 0; i < statJson->keyList.Length(); i++){
		stats[statJson->keyList[i]] = static_cast<JsonNumber*>(statJson->trie[statJson->keyList[i]])->number;
	}
	
	for (unsigned int i = 0; i < traitJson->keyList.Length(); i++){
		traits[traitJson->keyList[i]] = static_cast<JsonString*>(traitJson->trie[traitJson->keyList[i]])->string;
	}
	
	for (unsigned int i = 0; i < iconJson->Length(); i++){
		icons.Append(static_cast<JsonString*>(iconJson->array[i])->string;
	}
	
	delete buildingJson;
}