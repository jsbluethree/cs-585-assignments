// BuildingInfo.h
// Chris Bowers

#ifndef __BUILDINGINFO_H__
#define __BUILDINGINFO_H__

#include "JsonParser.h"
#include <map>
#include "DynamicArray.h"

class BuildingInfo{
public:
	std::map<std::string, float> stats;
	std::map<std::string, std::string> traits;
	DynamicArray<std::string> icons;
	
	void GetBuildingInfo(std::string filepath);
};

#endif
