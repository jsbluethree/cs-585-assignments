// ControllerFactory.cpp
// Chris Bowers

#include "ControllerFactory.h"

void ControllerFactory::AddInitActors(std::string filepath){
	JsonArray* actorJson = static_cast<JsonArray*>(JsonParser::GetInstance().Parse(filepath));
	for (unsigned int i = 0; i < actorJson->array.Length(); i++){
		std::string actortype = static_cast<JsonString*>(static_cast<JsonArray*>(actorJson->array[i])->array[0])->string;
		int xpos = static_cast<JsonNumber*>(static_cast<JsonArray*>(actorJson->array[i])->array[1])->number;
		int ypos = static_cast<JsonNumber*>(static_cast<JsonArray*>(actorJson->array[i])->array[2])->number;
		if (actortype == "dwarf"){
			dwarves.Append(new DwarfController(Info::GetInstance().actors["dwarf"])
			dwarves[dwarves.Length() - 1]->actor.SetX(xpos);
			dwarves[dwarves.Length() - 1]->actor.SetY(ypos);
			DwarfSceneManager::GetInstance().sceneGraph.AddNode(&dwarves[dwarves.Length() - 1]->actor);
			DwarfSceneManager::GetInstance().AddTickable(dwarves[dwarves.Length() - 1]);
			drawer->AddDrawable(&dwarves[dwarves.Length() - 1]->actor);
		}
		else if (actortype == "orc"){
			orcs.Append(new OrcController(Info::GetInstance().actors["orc"])
			orcs[orcs.Length() - 1]->actor.SetX(xpos);
			orcs[orcs.Length() - 1]->actor.SetY(ypos);
			DwarfSceneManager::GetInstance().sceneGraph.AddNode(&orcs[orcs.Length() - 1]->actor);
			DwarfSceneManager::GetInstance().AddTickable(orcs[orcs.Length() - 1]);
			drawer->AddDrawable(&orcs[orcs.Length() - 1]->actor);
		}
		else if (actortype == "greathall"){
			buildings.Append(new BuildingActor(Info::GetInstance().buildings["greathall"].stats, Info::GetInstance().buildings["greathall"].traits, Info::GetInstance().buildings["greathall"].icons));
			buildings[buildings.Length() - 1]->SetX(xpos);
			buildings[buildings.Length() - 1]->SetY(ypos);
			DwarfSceneManager::GetInstance().sceneGraph.AddNode(buildings[buildings.Length() - 1]);
			drawer->AddDrawable(buildings[buildings.Length() - 1]);			
		}
		// handle init cursor
}