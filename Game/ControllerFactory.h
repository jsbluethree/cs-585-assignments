// ControllerFactory.h
// Chris Bowers

#ifndef __CONTROLLERFACTORY_H__
#define __CONTROLLERFACTORY_H__

#include "DwarfSceneManager.h"
#include "DwarfController.h"
#include "OrcController.h"
#include "CursesDrawer.h"
#include "JsonParser.h"
#include "Info.h"
#include "BuildingActor.h"

class ControllerFactory{
public:
	CursesDrawer* drawer;
	DynamicArray<DwarfController*> dwarves;
	DynamicArray<OrcController*> orcs;
	DynamicArray<BuildingActor*> buildings;
	PlayerController* player;

	void AddInitActors(std::string filepath);
};

#endif
