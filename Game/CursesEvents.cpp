// CursesEvents.cpp
// Chris Bowers 10/31/2014

#include "CursesEvents.h"

CameraEvent::CameraEvent(int dir) : direction(dir) { type = "camera"; }

ActorMoveEvent::ActorMoveEvent(std::string name, int oldY, int oldX, int newY, int newX) : name(name), oldY(oldY), oldX(oldX), newY(newY), newX(newX) { type = "actormove"; }