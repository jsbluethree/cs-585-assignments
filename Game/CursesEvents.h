// CursesEvents.h
// Chris Bowers 10/31/2014

#ifndef __CURSESEVENTS_H__
#define __CURSESEVENTS_H__

#include "IEvent.h"

class CameraEvent : public IEvent{
public:
	int direction;
	
	CameraEvent(int dir);
};

class ActorMoveEvent : public IEvent{
public:
	std::string name;
	int oldY;
	int oldX;
	int newY;
	int newX;	
	
	ActorMoveEvent(std::string name, int oldY, int oldX, int newY, int newX);
};
	
#endif
