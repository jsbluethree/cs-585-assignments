// CursesGame.cpp
// Chris Bowers 10/25/2014

#include "CursesGame.h"

void CursesGame::ReadLevel(std::string filepath){
	// parse json
	JsonValue* levelJson = JsonParser::GetInstance()->Parse(filepath);
	// populate values from json
	level.SetWidth(static_cast<JsonNumber*>(static_cast<JsonObject*>(levelJson)->trie["width"])->number);
	level.SetHeight(static_cast<JsonNumber*>(static_cast<JsonObject*>(levelJson)->trie["height"])->number);
	unsigned int treecount = static_cast<JsonNumber*>(static_cast<JsonObject*>(levelJson)->trie["treecount"])->number;
	unsigned int mtncount = static_cast<JsonNumber*>(static_cast<JsonObject*>(levelJson)->trie["mtncount"])->number;
	unsigned int watercount = static_cast<JsonNumber*>(static_cast<JsonObject*>(levelJson)->trie["watercount"])->number;
	unsigned int grasscount = static_cast<JsonNumber*>(static_cast<JsonObject*>(levelJson)->trie["grasscount"])->number;
	std::string defaultTile = static_cast<JsonString*>(static_cast<JsonObject*>(levelJson)->trie["default"])->string;
	
	// fill tile array with default tile
	for (unsigned int i = 0; i < level.Width() * level.Height(); i++){
		level.tiles.Append(defaultTile);
	}
	
	// set random tiles to decorations
	unsigned int randpos;
	srand(time(nullptr));
	for (unsigned int i = 0; i < treecount; i++){
		randpos = rand() % level.tiles.Length();
		level.tiles[randpos] = "tree";
	}
	
	for (unsigned int i = 0; i < mtncount; i++){
		randpos = rand() % level.tiles.Length();
		level.tiles[randpos] = "mount";
	}
	
	for (unsigned int i = 0; i < watercount; i++){
		randpos = rand() % level.tiles.Length();
		level.tiles[randpos] = "water";
	}
	
	for (unsigned int i = 0; i < grasscount; i++){
		randpos = rand() % level.tiles.Length();
		level.tiles[randpos] = "grass";
	}
	delete levelJson;
}

void CursesGame::Init(){
	// initialize ncurses
	initscr();
	// don't buffer inputs
	cbreak();
	// don't echo inputs
	noecho();
	// don't delay on inputs
	nodelay(stdscr, TRUE);
	// enable keypad inputs
	keypad(stdscr, TRUE);
	// start color
	start_color();
	
	return;
}

void CursesGame::Run(){
	// do some things here
	// initialize level
	renderer.level = &level;
	renderer.InitLevel();
	
	// create player controller and actor
	PlayerActor player;
	PlayerController playerController(&player, &level);
	
	// create orc and dwarf spawners and set them up
	OrcSpawner orcSpawner;
	orcSpawner.level = &level;
	orcSpawner.renderer = &renderer;
	orcSpawner.player = &playerController;
	
	DwarfSpawner dwarfSpawner;
	dwarfSpawner.level = &level;
	dwarfSpawner.renderer = &renderer;
	dwarfSpawner.player = &playerController;

	// set cursor to 0, 0
	player.SetX(0);
	player.SetY(0);
	
	// register renderer listener on playerController camera and select states
	playerController.stateMachine.stateLookup["camera"]->events.AddListener("camera", &renderer.cameraMoveListener);
	playerController.stateMachine.stateLookup["select"]->events.AddListener("actormove", &renderer.actorMoveListener);
	
	// add tickables to scene manager
	SceneManager::GetInstance()->AddTickable(&playerController);
	SceneManager::GetInstance()->AddTickable(&orcSpawner);
	SceneManager::GetInstance()->AddTickable(&dwarfSpawner);
	SceneManager::GetInstance()->AddTickable(&playerController.stateMachine.stateLookup["camera"]->events);
	SceneManager::GetInstance()->AddTickable(&playerController.stateMachine.stateLookup["select"]->events);
	SceneManager::GetInstance()->AddTickable(&playerController.stateMachine.stateLookup["interact"]->events);
	SceneManager::GetInstance()->AddTickable(&renderer);
	
	
	// MAIN GAME LOOP
	auto startClock = std::chrono::high_resolution_clock::now();
	int ch;
	// while escape is not pressed
	while ((ch = getch()) != 27){
		// put the input back into the queue if there was one
		if (ch != ERR)
			ungetch(ch);
		// populate info window
		werase(renderer.infoWindow);
		mvwprintw(renderer.infoWindow, 0, 0, "# of Orcs: %u Next spawn in: %i", orcSpawner.controllers.Length(), (int)(orcSpawner.nextSpawn - orcSpawner.spawnTimer));
		mvwprintw(renderer.infoWindow, 1, 0, "# of Dwarves: %u Next spawn in: %i",dwarfSpawner.controllers.Length(), (int)(dwarfSpawner.nextSpawn - orcSpawner.spawnTimer));
		if (playerController.stateMachine.currentState == playerController.stateMachine.stateLookup["interact"]){
			DynamicArray<SceneNode*> cursorCollider = level.GetColliders(player.GetX(), player.GetY());
			if (!cursorCollider.IsEmpty()){
				if (cursorCollider[0]->name == "orc")
					mvwprintw(renderer.infoWindow, 2, 0, "This is an orc. It distrusts mages.");
				else
					mvwprintw(renderer.infoWindow, 2, 0, "This is a dwarf. It menaces with spikes of steel.");
			}
		}
		
		
		// tick all tickables
		SceneManager::GetInstance()->Tick(std::chrono::duration<float>(std::chrono::high_resolution_clock::now() - startClock).count());
	}
}

void CursesGame::Exit(){
	// end ncurses
	endwin();
	return;
}
