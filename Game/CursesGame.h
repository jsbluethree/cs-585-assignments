// CursesGame.h
// Chris Bowers 10/25/2014

#ifndef __CURSESGAME_H__
#define __CURSESGAME_H__

#include <stdlib.h>
#include <fstream>
#include <ncurses.h>
#include <chrono>
#include "SceneManager.h"
#include "ICallback.h"
#include "JsonParser.h"
#include "CursesLevel.h"
#include "PlayerController.h"
#include "CursesRenderer.h"
#include "OrcSpawner.h"
#include "DwarfSpawner.h"

class CursesGame{
private:

public:
	CursesLevel level;			// store level information
	CursesRenderer renderer;	// renders graphics
	
	void ReadLevel(std::string filepath);	// read level information from a json file
	
	void Init();	// initialize level and ncurses	
	void Run();		// contains main game loop
	void Exit();	// cleans up
	
};

#endif
