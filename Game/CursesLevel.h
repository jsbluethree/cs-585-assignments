// CursesLevel.h
// Chris Bowers 10/27/2014

#ifndef __CURSESLEVEL_H__
#define __CURSESLEVEL_H__

#include "DynamicArray.h"
#include "FixedGrid.h"
#include <string>

class CursesLevel : public FixedGrid{
public:
	DynamicArray<std::string> tiles;
};

#endif
