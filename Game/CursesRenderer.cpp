// CursesRenderer.cpp
// Chris Bowers 10/29/2014

#include "CursesRenderer.h"

#define DIRT_COLOR 3
#define TREE_COLOR 5
#define ORC_COLOR 1
#define DWARF_COLOR 6
#define WATER_COLOR 4
#define MTN_COLOR 7
#define GRASS_COLOR 2

CursesRenderer::CursesRenderer() {
	cameraMoveListener.renderer = this;
	actorMoveListener.renderer = this;
}

CursesRenderer::~CursesRenderer(){}

void CursesRenderer::InitLevel(){
	JsonValue* renderInfo = JsonParser::GetInstance()->Parse("Game/rendering.json");
	// populate icons map with chars and colors
	// get ready for insane Json extracting
	// note that this can be made generic by iterating through the KeyList in the JsonObject
	JsonArray* dirtColor = static_cast<JsonArray*>(static_cast<JsonObject*>(static_cast<JsonObject*>(renderInfo)->trie["dirt"])->trie["color"]);
	init_color(DIRT_COLOR, static_cast<JsonNumber*>(dirtColor->array[0])->number, static_cast<JsonNumber*>(dirtColor->array[1])->number, static_cast<JsonNumber*>(dirtColor->array[2])->number);
	init_pair(DIRT_COLOR, DIRT_COLOR, COLOR_BLACK);
	icons["dirt"] = static_cast<JsonString*>(static_cast<JsonObject*>(static_cast<JsonObject*>(renderInfo)->trie["dirt"])->trie["char"])->string[0] | COLOR_PAIR(DIRT_COLOR);
	
	JsonArray* treeColor = static_cast<JsonArray*>(static_cast<JsonObject*>(static_cast<JsonObject*>(renderInfo)->trie["tree"])->trie["color"]);
	init_color(TREE_COLOR, static_cast<JsonNumber*>(treeColor->array[0])->number, static_cast<JsonNumber*>(treeColor->array[1])->number, static_cast<JsonNumber*>(treeColor->array[2])->number);
	init_pair(TREE_COLOR, TREE_COLOR, COLOR_BLACK);
	icons["tree"] = static_cast<JsonString*>(static_cast<JsonObject*>(static_cast<JsonObject*>(renderInfo)->trie["tree"])->trie["char"])->string[0] | COLOR_PAIR(TREE_COLOR);
	
	JsonArray* orcColor = static_cast<JsonArray*>(static_cast<JsonObject*>(static_cast<JsonObject*>(renderInfo)->trie["orc"])->trie["color"]);
	init_color(ORC_COLOR, static_cast<JsonNumber*>(orcColor->array[0])->number, static_cast<JsonNumber*>(orcColor->array[1])->number, static_cast<JsonNumber*>(orcColor->array[2])->number);
	init_pair(ORC_COLOR, ORC_COLOR, COLOR_BLACK);
	icons["orc"] = static_cast<JsonString*>(static_cast<JsonObject*>(static_cast<JsonObject*>(renderInfo)->trie["orc"])->trie["char"])->string[0] | COLOR_PAIR(ORC_COLOR);
	
	JsonArray* dwarfColor = static_cast<JsonArray*>(static_cast<JsonObject*>(static_cast<JsonObject*>(renderInfo)->trie["dwarf"])->trie["color"]);
	init_color(DWARF_COLOR, static_cast<JsonNumber*>(dwarfColor->array[0])->number, static_cast<JsonNumber*>(dwarfColor->array[1])->number, static_cast<JsonNumber*>(dwarfColor->array[2])->number);
	init_pair(DWARF_COLOR, DWARF_COLOR, COLOR_BLACK);
	icons["dwarf"] = static_cast<JsonString*>(static_cast<JsonObject*>(static_cast<JsonObject*>(renderInfo)->trie["dwarf"])->trie["char"])->string[0] | COLOR_PAIR(DWARF_COLOR);
	
	JsonArray* waterColor = static_cast<JsonArray*>(static_cast<JsonObject*>(static_cast<JsonObject*>(renderInfo)->trie["water"])->trie["color"]);
	init_color(WATER_COLOR, static_cast<JsonNumber*>(waterColor->array[0])->number, static_cast<JsonNumber*>(waterColor->array[1])->number, static_cast<JsonNumber*>(waterColor->array[2])->number);
	init_pair(WATER_COLOR, WATER_COLOR, COLOR_BLACK);
	icons["water"] = static_cast<JsonString*>(static_cast<JsonObject*>(static_cast<JsonObject*>(renderInfo)->trie["water"])->trie["char"])->string[0] | COLOR_PAIR(WATER_COLOR);
	
	JsonArray* mountColor = static_cast<JsonArray*>(static_cast<JsonObject*>(static_cast<JsonObject*>(renderInfo)->trie["mount"])->trie["color"]);
	init_color(MTN_COLOR, static_cast<JsonNumber*>(mountColor->array[0])->number, static_cast<JsonNumber*>(mountColor->array[1])->number, static_cast<JsonNumber*>(mountColor->array[2])->number);
	init_pair(MTN_COLOR, MTN_COLOR, COLOR_BLACK);
	icons["mount"] = static_cast<JsonString*>(static_cast<JsonObject*>(static_cast<JsonObject*>(renderInfo)->trie["mount"])->trie["char"])->string[0] | COLOR_PAIR(MTN_COLOR);
	
	JsonArray* grassColor = static_cast<JsonArray*>(static_cast<JsonObject*>(static_cast<JsonObject*>(renderInfo)->trie["grass"])->trie["color"]);
	init_color(GRASS_COLOR, static_cast<JsonNumber*>(grassColor->array[0])->number, static_cast<JsonNumber*>(grassColor->array[1])->number, static_cast<JsonNumber*>(grassColor->array[2])->number);
	init_pair(GRASS_COLOR, GRASS_COLOR, COLOR_BLACK);
	icons["grass"] = static_cast<JsonString*>(static_cast<JsonObject*>(static_cast<JsonObject*>(renderInfo)->trie["grass"])->trie["char"])->string[0] | COLOR_PAIR(GRASS_COLOR);
	
	// add cursor icon
	init_pair(8, COLOR_WHITE, COLOR_BLACK);
	icons["cursor"] = 'X' | COLOR_PAIR(8);
	
	// create level window
	levelWindow = newpad(level->Height(), level->Width());
	
	// set initial camera position
	cameraOffsetX = 0;
	cameraOffsetY = 0;
	
	// set window background
	wmove(levelWindow, 0, 0);
	for (unsigned int i = 0; i < level->tiles.Length(); i++){
		waddch(levelWindow, icons[level->tiles[i]]);
	}
	
	// create info window
	infoWindow = newwin(3, COLS, LINES - 3, 0);
}

void CursesRenderer::Tick(float dt){
	// write level window to screen
	pnoutrefresh(levelWindow, cameraOffsetY, cameraOffsetX, 0, 0, std::min(LINES - 4, (int)level->Height()), std::min(COLS - 1, (int)level->Width()));
	// write info window to screen
	wnoutrefresh(infoWindow);
	// draw to screen
	doupdate();
}

void CursesRenderer::OnCameraMove::Execute(IEvent* event){
	switch (static_cast<CameraEvent*>(event)->direction) {
	case KEY_UP:
		if (renderer->cameraOffsetY > 0)
			renderer->cameraOffsetY--;
		break;
	case KEY_DOWN:
		if (renderer->cameraOffsetY < renderer->level->Height() - LINES)
			renderer->cameraOffsetY++;
		break;
	case KEY_LEFT:
		if (renderer->cameraOffsetX > 0)
			renderer->cameraOffsetX--;
		break;
	case KEY_RIGHT:
		if (renderer->cameraOffsetX < renderer->level->Width() - COLS)
			renderer->cameraOffsetX++;
		break;	
	}
}

void CursesRenderer::OnActorMove::Execute(IEvent* event){
	ActorMoveEvent* actorEvent = static_cast<ActorMoveEvent*>(event);
	// render the actor's icon at the new position
	mvwaddch(renderer->levelWindow, actorEvent->newY, actorEvent->newX, renderer->icons[actorEvent->name]);
	// render the background tile at the old position
	mvwaddch(renderer->levelWindow, actorEvent->oldY, actorEvent->oldX, renderer->icons[renderer->level->tiles[actorEvent->oldY * renderer->level->Width() + actorEvent->oldX]]);
}