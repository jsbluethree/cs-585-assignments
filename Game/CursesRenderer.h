// CursesRenderer.h
// Chris Bowers 10/29/2014

#ifndef __CURSESRENDERER_H__
#define __CURSESRENDERER_H__

#include <ncurses.h>
#include <map>
#include <fstream>
#include <algorithm>
#include "DynamicArray.h"
#include "ICallback.h"
#include "ITickable.h"
#include "CursesLevel.h"
#include "JsonParser.h"
#include "CursesEvents.h"

class CursesRenderer : public ITickable{
public:
	WINDOW* levelWindow;
	WINDOW* infoWindow;
	CursesLevel* level;
	std::map<std::string, chtype> icons;
	
	unsigned int cameraOffsetX;
	unsigned int cameraOffsetY;
	
	class OnCameraMove : public ICallback{
	public:
		CursesRenderer* renderer;
		void Execute(IEvent* event);
	} cameraMoveListener;
	
	class OnActorMove : public ICallback{
	public:
		CursesRenderer* renderer;
		void Execute(IEvent* event);
	} actorMoveListener;
	
	CursesRenderer();
	~CursesRenderer();
	
	void InitLevel();
	void Tick(float dt);	
};

#endif
