// DwarfActor.h
// Chris Bowers 11/3/2014

#ifndef __DWARFACTOR_H__
#define __DWARFACTOR_H__

#include "SceneNode.h"

class DwarfActor : public SceneNode{
public:
	DwarfActor();
};

#endif
