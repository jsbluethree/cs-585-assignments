// DwarfController.cpp
// Chris Bowers 11/10/2014

#include "DwarfController.h"

DwarfController::DwarfController(ActorInfo& dwarfInfo) : actor(dwarfInfo.stats, dwarfInfo.traits) {
	stateMachine.stateLookup["idle"] = new DwarfIdleState(&actor);
	stateMachine.stateLookup["moveto"] = new DwarfMoveToState(&actor);
	stateMachine.stateLookup["drink"] = new DwarfDrinkingState(&actor);
	stateMachine.stateLookup["sleep"] = new DwarfSleepingState(&actor);
	stateMachine.stateLookup["hunt"] = new DwarfHuntingState(&actor);
	stateMachine.stateLookup["combat"] = new DwarfCombatState(&actor);
	stateMachine.stateLookup["buy"] = new DwarfBuyingState(&actor);
	
	stateMachine.currentState = stateMachine.stateLookup["idle"];
	stateMachine.currentState->events.AddListener("state", &stateMachine.stateTransitionListener);
}

DwarfController::~DwarfController(){
	delete stateMachine.stateLookup["idle"];
	delete stateMachine.stateLookup["moveto"];
	delete stateMachine.stateLookup["drink"];
	delete stateMachine.stateLookup["sleep"];
	delete stateMachine.stateLookup["hunt"];
	delete stateMachine.stateLookup["combat"];
	delete stateMachine.stateLookup["buy"];
}

void DwarfController::Tick(float dt) { stateMachine.currentState->Run(); }

DwarfIdleState(GenericActor* actor) : actor(actor) {}

DwarfMoveToState(GenericActor* actor) : actor(actor) {}

DwarfDrinkingState(GenericActor* actor) : actor(actor) {}

DwarfSleepingState(GenericActor* actor) : actor(actor) {}

DwarfHuntingState(GenericActor* actor) : actor(actor) {}

DwarfCombatState(GenericActor* actor) : actor(actor) {}

DwarfBuyingState(GenericActor* actor) : actor(actor) {}

void DwarfIdleState::Run(){

}

void DwarfMoveToState::Run(){

}

void DwarfDrinkingState::Run(){

}

void DwarfSleepingState::Run(){

}

void DwarfHuntingState::Run(){

}

void DwarfCombatState::Run(){

}

void DwarfBuyingState::Run(){

}