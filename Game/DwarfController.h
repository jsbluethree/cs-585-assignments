// DwarfController.h
// Chris Bowers 11/9/2014

#ifndef __DWARFCONTROLLER_H__
#define __DWARFCONTROLLER_H__

#include "GenericActor.h"
#include "ActorInfo.h"
#include "StateMachine.h"
#include "ITickable.h"
#include "DwarfSceneManager.h"

class DwarfController : public ITickable{
public:
	StateMachine stateMachine;
	GenericActor actor;
	
	DwarfController(ActorInfo& dwarfInfo);
	~DwarfController();
	
	void Tick(float dt);
};

class DwarfIdleState : public IState{
public:
	GenericActor* actor;
	DwarfIdleState(GenericActor* actor);
	void Run();
};

class DwarfMoveToState : public IState{
public:
	GenericActor* actor;
	DwarfMoveToState(GenericActor* actor);
	void Run();
};

class DwarfDrinkingState : public IState{
public:
	GenericActor* actor;
	DwarfDrinkingState(GenericActor* actor);
	void Run();
};

class DwarfSleepingState : public IState{
public:
	GenericActor* actor;
	DwarfSleepingState(GenericActor* actor);
	void Run();
};

class DwarfHuntingState : public IState{
public:
	GenericActor* actor;
	DwarfHuntingState(GenericActor* actor);
	void Run();
};

class DwarfCombatState : public IState{
public:
	GenericActor* actor;
	DwarfCombatState(GenericActor* actor);
	void Run();
};

class DwarfBuyingState : public IState{
public:
	GenericActor* actor;
	DwarfBuyingState(GenericActor* actor);
	void Run();
};

#endif
