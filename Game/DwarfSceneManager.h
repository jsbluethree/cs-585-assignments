// DwarfSceneManager.h
// Chris Bowers 11/7/2014

#ifndef __DWARFSCENEMANAGER_H__
#define __DWARFSCENEMANAGER_H__

#include "SceneManager.h"
#include "FixedGrid.h"

class DwarfSceneManager : public SceneManager{
public:
	FixedGrid sceneGraph;
};

#endif
