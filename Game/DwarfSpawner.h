// DwarfSpawner.h
// Chris Bowers 11/3/2014

#ifndef __DWARFSPAWNER_H__
#define __DWARFSPAWNER_H__

#include <stdlib.h>
#include "DynamicArray.h"
#include "DwarfController.h"
#include "ITickable.h"
#include "CursesLevel.h"
#include "SceneManager.h"
#include "CursesRenderer.h"
#include "PlayerController.h"

class DwarfSpawner : public ITickable{
public:
	DynamicArray<DwarfController*> controllers;
	float lastTick;
	float nextSpawn;
	float spawnTimer;
	CursesLevel* level;
	CursesRenderer* renderer;
	PlayerController* player;
	
	DwarfSpawner();
	~DwarfSpawner();
	
	void Tick(float dt);
};

#endif
