// DwarfUI.cpp
// Chris Bowers

#include "DwarfUI.h"

void DwarfUI::Tick(float dt){
	werase(textWindow);
	mvwprintw(textWindow, 0, 0, "%i G", (int)player->actor.stats[gold]);
	
	if (player->stateMachine.currentState == player->stateMachine.stateLookup["selection"]){
		if (player->stateMachine.stateLookup["selection"]->dwarf){
			mvwprintw(textWindow, 0, 8, "1: Order this dwarf? %i G", (int)player->stateMachine.stateLookup["selection"]->dwarf->stats["ordercost"]);
		}
		if (player->stateMachine.stateLookup["selection"]->building){
			if (player->stateMachine.stateLookup["selection"]->building->stats["level"] < 3){
				mvwprintw(textWindow, 1, 8, "2: Upgrade this building? %i G", (int)player->stateMachine.stateLookup["selection"]->building->stats["upgradecost"]);
			}
			if (player->stateMachine.stateLookup["selection"]->building->traits["type"] == "greathall"){
				mvwprintw(textWindow, 2, 8, "3: Build an apothecary? %i G", (int)player->stateMachine.stateLookup["selection"]->building->stats["apothecary"]);
				mvwprintw(textWindow, 3, 8, "4: Build a blacksmith? %i G", (int)player->stateMachine.stateLookup["selection"]->building->stats["blacksmith"]);
			}
		}
	}
	
	else if (player->stateMachine.currentState == player->stateMachine.stateLookup["dwarforder"]){
		mvwprintw(textWindow, 0, 8, "1: Move here?");
	}
	
	else if (player->stateMachine.currentState == player->stateMachine.stateLookup["buildapothecary"]){
		mvwprintw(textWindow, 0, 8, "1: Build apothecary here?");
	}
	
	else if (player->stateMachine.currentState == player->stateMachine.stateLookup["buildblacksmith"]){
		mvwprintw(textWindow, 0, 8, "1: Build blacksmith here?");
	}
	
	wnoutrefresh(textWindow);
}