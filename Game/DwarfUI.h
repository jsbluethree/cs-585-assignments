// DwarfUI.h
// Chris Bowers

#ifndef __DWARFUI_H__
#define __DWARFUI_H__

#include "ITickable.h"
#include "PlayerController.h"
#include <ncurses.h>

class DwarfUI : public ITickable{
public:
	WINDOW* textWindow;
	PlayerController* player;
	
	

	void Tick(float dt);
};

#endif
