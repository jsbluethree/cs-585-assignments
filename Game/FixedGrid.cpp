// FixedGrid.cpp
// 9/12/2014

#include "FixedGrid.h"

FixedGrid::FixedGrid(unsigned int w, unsigned int h){
	width = w;
	height = h;
}

std::string FixedGrid::Map(int x, int y){
	std::string outstring = "";
	unsigned int value = x + width * y;
	if (value > width * height)
		return outstring;
	else if (value == 0)
		outstring = "a";
	while (value > 0){
		int remainder = value % 26;
		outstring = (char)(remainder + 'a') + outstring;
		value = (value - remainder) / 26;
	}
	return outstring;
}

void FixedGrid::SetWidth(unsigned int w) { width = w; }

void FixedGrid::SetHeight(unsigned int h) { height = h; }

unsigned int FixedGrid::Width() { return width; }

unsigned int FixedGrid::Height() { return height; }