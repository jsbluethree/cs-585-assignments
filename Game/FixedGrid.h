// FixedGrid.h
// Chris Bowers 9/10/2014

#ifndef __FIXEDGRID_H__
#define __FIXEDGRID_H__

#include "ISceneGraph.h"

class FixedGrid: public ISceneGraph{
private:
	unsigned int width;
	unsigned int height;
public:
	FixedGrid(unsigned int w = 10, unsigned int h = 10);	// constructor
	std::string Map(int x, int y);							// maps a coordinate point to a base-26 integer represented as a string

	void SetWidth(unsigned int w);
	void SetHeight(unsigned int h);
	
	unsigned int Width();
	unsigned int Height();
	
	
};

#endif

