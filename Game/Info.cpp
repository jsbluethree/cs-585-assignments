// Info.cpp
// Chris Bowers

#include "Info.h"

Info* Info::instance = nullptr;

Info::Info() {}

Info& Info::GetInstance(){
	if (!instance)
		instance = new Info();
	return *instance;
}