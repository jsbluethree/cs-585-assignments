// Info.h
// Chris Bowers

#ifndef __INFO_H__
#define __INFO_H__

#include "ActorInfo.h"
#include "BuildingInfo.h"
#include <map>

class Info{
private:
	static Info* instance;
	Info();
public:
	std::map<std::string, ActorInfo> actors;
	std::map<std::string, BuildingInfo> buildings;
	
	static Info& GetInstance();
};

#endif
