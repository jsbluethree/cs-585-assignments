// MonsterController.cpp 
// Chris Bowers 9/25/2014

#include "MonsterController.h"

DeathEvent::DeathEvent(){ type = "death"; }

MonsterController::MonsterController(Pawn* monster) : pawn(monster){
	monsterKilledListener.controller = this;
	// this is inefficient since it creates an extra event object, but I just wanted to show that it can be done
	pawn->events.AddListener(DeathEvent().Type(), &monsterKilledListener);
	SceneManager::GetInstance()->AddTickable(this);
}

void MonsterController::OnMonsterKilled::Execute(IEvent* event){
	SceneManager::GetInstance()->RemoveTickable(controller);
}

void MonsterController::Tick(float dt){
	pawn->health--;
	std::cout << "MC::Tick() called ";
	if (pawn->health == 0){
		pawn->events.AddEvent(new DeathEvent);
		// if you wanted the event to trigger immediately, you would instead write:
		// pawn->events.Dispatch(new DeathEvent);
	}
}