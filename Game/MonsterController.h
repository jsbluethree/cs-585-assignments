// MonsterController.h 
// Chris Bowers 9/25/2014

#ifndef __MONSTERCONTROLLER_H__
#define __MONSTERCONTROLLER_H__

#include "ICallback.h"
#include "IEvent.h"
#include "ITickable.h"
#include "Pawn.h"
#include "SceneManager.h"

class DeathEvent : public IEvent{
public:
	DeathEvent();
};

class MonsterController : public ITickable{
private:
	Pawn* pawn;	// pointer to controlled actor

	class OnMonsterKilled : public ICallback{
	public:
		MonsterController* controller;
		void Execute(IEvent* event);	// removes controller from tickList
	} monsterKilledListener;			// listener for death events

public:
	MonsterController(Pawn* monster);	// constructor
	void Tick(float dt);				// for test case, decrements health, then sends a death event if health = 0
};

#endif
