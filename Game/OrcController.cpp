// OrcController.cpp
// Chris Bowers 11/11/2014

#include "OrcController.h"

OrcController::OrcController(ActorInfo& orcInfo) : actor(orcInfo.stats, orcInfo.traits) {
	stateMachine.stateLookup["idle"] = new OrcIdleState(&actor);
	stateMachine.stateLookup["combat"] = new OrcCombatState(&actor);
	
	stateMachine.currentState = stateMachine.stateLookup["idle"];
	stateMachine.currentState->events.AddListener("state", &stateMachine.stateTransitionListener);	
}

OrcController::~OrcController(){
	delete stateMachine.stateLookup["idle"];
	delete stateMachine.stateLookup["combat"];
}

void OrcController::Tick(float dt) { stateMachine.currentState->Run(); }

OrcIdleState::OrcIdleState(GenericActor* actor) : actor(actor) {}

OrcCombatState::OrcCombatState(GenericActor* actor) : actor(actor) {}

void OrcIdleState::Run(){

}

void OrcCombatState::Run(){

}