// OrcController.h
// Chris Bowers 11/11/2014

#ifndef __ORCCONTROLLER_H__
#define __ORCCONTROLLER_H__

#include "GenericActor.h"
#include "ActorInfo.h"
#include "StateMachine.h"
#include "ITickable.h"
#include "DwarfSceneManager.h"

class OrcController : public ITickable{
public:
	StateMachine stateMachine;
	GenericActor actor;
	
	OrcController(ActorInfo& orcInfo);
	~OrcController();
	
	void Tick(float dt);
};

class OrcIdleState : public IState{
public:
	GenericActor* actor;
	OrcIdleState(GenericActor* actor);
	void Run();
};

class OrcCombatState : public IState{
public:
	GenericActor* actor;
	OrcCombatState(GenericActor* actor);
	void Run();
};

#endif
