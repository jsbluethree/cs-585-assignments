// OrcSpawner.cpp
// Chris Bowers 11/2/2014

#include "OrcSpawner.h"

OrcSpawner::OrcSpawner() : lastTick(0), spawnTimer(0) { nextSpawn = 5 + rand() % 11; }

OrcSpawner::~OrcSpawner() {
	// delete all controllers
	for (unsigned int i = 0; i < controllers.Length(); i++){
		delete controllers[i];
	}
}

void OrcSpawner::Tick(float dt){
	// increment spawn timer if game is not paused
	if (player->stateMachine.currentState == player->stateMachine.stateLookup["camera"]){
		spawnTimer += dt - lastTick;
	}
	if (spawnTimer >= nextSpawn){
		// add it to the controllers list
		// the controller constructor will take care of adding the actor to the SceneGraph
		controllers.Append(new OrcController(level, player));
		// set up renderer listener on the dispatcher
		controllers[controllers.Length() - 1]->events.AddListener("actormove", &renderer->actorMoveListener);
		// add the controller and actor to the scene manager
		SceneManager::GetInstance()->AddSceneNode(&controllers[controllers.Length() - 1]->actor);
		SceneManager::GetInstance()->AddTickable(controllers[controllers.Length() - 1]);
		SceneManager::GetInstance()->AddTickable(&controllers[controllers.Length() - 1]->events);
		nextSpawn = 5 + rand() % 11;
		spawnTimer = 0;
	}
	lastTick = dt;
}