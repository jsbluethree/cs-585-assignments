// OrcSpawner.h
// Chris Bowers 10/29/2014

#ifndef __ORCSPAWNER_H__
#define __ORCSPAWNER_H__

#include <stdlib.h>
#include "DynamicArray.h"
#include "OrcController.h"
#include "ITickable.h"
#include "CursesLevel.h"
#include "SceneManager.h"
#include "CursesRenderer.h"
#include "PlayerController.h"

class OrcSpawner : public ITickable{
public:
	DynamicArray<OrcController*> controllers;
	float lastTick;
	float nextSpawn;
	float spawnTimer;
	CursesLevel* level;
	CursesRenderer* renderer;
	PlayerController* player;
	
	OrcSpawner();
	~OrcSpawner();
	
	void Tick(float dt);
};

#endif
