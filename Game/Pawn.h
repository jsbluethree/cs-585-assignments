// Pawn.h 
// Chris Bowers 9/25/2014

#ifndef __PAWN_H__
#define __PAWN_H__

#include "Dispatcher.h"
#include "SceneManager.h"

class Pawn{
public:
	Dispatcher events;		// event dispatcher
	int health;
	Pawn(int health = 10);	// constructor
};

#endif
