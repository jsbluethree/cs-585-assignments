// PlayerActor.h
// Chris Bowers 10/28/2014

#ifndef __PLAYERACTOR_H__
#define __PLAYERACTOR_H__

#include "SceneNode.h"
#include <map>

class PlayerActor : public SceneNode {
public:
	std::map<std::string, float> behavior;
	PlayerActor();
};

#endif
