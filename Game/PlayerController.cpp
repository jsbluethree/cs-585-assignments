// PlayerController.cpp
// Chris Bowers

#include "PlayerController.h"

PlayerController::PlayerController(ActorInfo& cursorInfo) : actor(cursorInfo.stats, cursorInfo.traits) {
	stateMachine.stateLookup["movement"] = new PlayerMovementState(&actor);
	stateMachine.stateLookup["selection"] = new PlayerSelectionState(&actor);
	stateMachine.stateLookup["dwarforder"] = new PlayerDwarfOrderState(&actor);
	stateMachine.stateLookup["buildapothecary"] = new PlayerBuildApothecaryState(&actor);
	stateMachine.stateLookup["buildblacksmith"] = new PlayerBuildBlacksmithState(&actor);
	
	stateMachine.currentState = stateMachine.stateLookup["movement"];
	stateMachine.currentState->events.AddListener("state", &stateMachine.stateTransitionListener);
}

PlayerController::~PlayerController() {
	delete stateMachine.stateLookup["movement"];
	delete stateMachine.stateLookup["selection"];
	delete stateMachine.stateLookup["dwarforder"];
	delete stateMachine.stateLookup["buildapothecary"];
	delete stateMachine.stateLookup["buildblacksmith"];
}

void PlayerController::Tick(float dt) { stateMachine.currentState->Run(); }

PlayerMovementState(GenericActor* actor) : actor(actor) {}

PlayerSelectionState(GenericActor* actor) : actor(actor) {}

PlayerDwarfOrderState(GenericActor* actor) : actor(actor) {}

PlayerBuildOrderState(GenericActor* actor) : actor(actor) {}

void PlayerMovementState::Run() {

}

void PlayerSelectionState::Enter(){

}

void PlayerSelectionState::Run() {

}

void PlayerSelectionState::Exit(){

}

void PlayerDwarfOrderState::Run() {

}

void PlayerBuildApothecaryState::Run() {

}

void PlayerBuildBlacksmithState::Run() {

}