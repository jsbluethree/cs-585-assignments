// PlayerController.h
// Chris Bowers

#ifndef __PLAYERCONTROLLER_H__
#define __PLAYERCONTROLLER_H__

#include "GenericActor.h"
#include "ActorInfo.h"
#include "StateMachine.h"
#include "Input.h"
#include "ITickable.h
#include "DwarfSceneManager.h"

class PlayerController : public ITickable{
public:
	StateMachine stateMachine;
	GenericActor actor;
	
	PlayerController(ActorInfo& cursorInfo);
	~PlayerController();
	
	void Tick(float dt);
};

class PlayerMovementState : public IState{
public:
	GenericActor* actor;
	PlayerMovementState(GenericActor* actor);
	void Run();
};

class PlayerSelectionState : public IState{
public:
	GenericActor* actor;
	GenericActor* dwarf;
	BuildingActor* building;
	PlayerSelectionState(GenericActor* actor);
	void Enter();
	void Run();
	void Exit();
};

class PlayerDwarfOrderState : public IState{
public:
	GenericActor* actor;
	PlayerDwarfOrderState(GenericActor* actor);
	void Run();
};

class PlayerBuildApothecaryState : public IState{
public:
	GenericActor* actor;
	PlayerBuildApothecaryState(GenericActor* actor);
	void Run();
};

class PlayerBuildBlacksmithState : public IState{
public:
	GenericActor* actor;
	PlayerBuildBlacksmithState(GenericActor* actor);
	void Run();
};



#endif
