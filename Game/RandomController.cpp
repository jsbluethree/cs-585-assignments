// RandomController.cpp
// Chris Bowers 9/11/2014

#include "RandomController.h"

RandomController::RandomController(FixedGrid* pgrid, SceneManager* pmanager){
	grid = pgrid;
	manager = pmanager;
	debug = Debug::GetInstance();
	manager->AddTickable(this);
}

RandomController::~RandomController(){
	for (unsigned int i = 0; i < actorList.Length(); ++i){
		delete actorList[i];
	}
		
}

void RandomController::NewActor(std::string label){
	int x = rand() % 10;
	int y = rand() % 10;
	Actor* newp = new Actor(label, x , y);
	actorList.Append(newp);
	manager->AddSceneNode(newp);
	grid->AddNode(newp);
	debug->Log("[Gameplay] Actor " + label + " spawned at " + std::to_string(x) + ", " + std::to_string(y));
}

void RandomController::RemoveActor(SceneNode* obj){
	for (unsigned int i = 0; i < actorList.Length(); ++i){
		if (obj == actorList[i]){
			manager->RemoveSceneNode(actorList[i]);
			grid->RemoveNode(actorList[i]);
			delete actorList[i];
			actorList.Delete(i);
			return;
		}
	}
}

void RandomController::Tick(float dt){
	for (unsigned int i = 0; i < actorList.Length(); ++i){
		// get a random direction x or y, and a random magnitude -1 or +1
		// technically -1 and +1 have the same magnitude but whatever
		int direction = rand() % 2;
		int magnitude;
		if (direction == 0){
			if (actorList[i]->GetX() == 0)
				magnitude = 1;
			else if (actorList[i]->GetX() == 9)
				magnitude = -1;
			else{
				magnitude = rand() % 2;
				if (magnitude == 0)
					magnitude = -1;
			}
			grid->UpdateNode(actorList[i], actorList[i]->GetX() + magnitude, actorList[i]->GetY());
		}
		else{
			if (actorList[i]->GetY() == 0)
				magnitude = 1;
			else if (actorList[i]->GetY() == 9)
				magnitude = -1;
			else{
				magnitude = rand() % 2;
				if (magnitude == 0)
					magnitude = -1;
			}
			grid->UpdateNode(actorList[i], actorList[i]->GetX(), actorList[i]->GetY() + magnitude);
		}
	}
	// if there are at least 2 actors, check if any are colliding, and if so, remove them
	if (actorList.Length() >= 2){
		for (unsigned int i = 0; i < actorList.Length(); ++i){
			DynamicArray<SceneNode*> colliding = grid->GetColliders(actorList[i]);
			if (colliding.Length() >= 2){
				for (unsigned int j = 0; j < colliding.Length(); ++j){
					debug->Log("[Gameplay] Actor " + colliding[j]->name + " collided at " + std::to_string(colliding[j]->GetX()) + ", " + std::to_string(colliding[j]->GetY()));
					RemoveActor(colliding[j]);
				}
			}
		}
	}
}

void RandomController::ActorList(){
	for (unsigned int i = 0; i < actorList.Length(); ++i){
		debug->Log("[Gameplay] Actor " + actorList[i]->name + " exists at " + std::to_string(actorList[i]->GetX()) + ", " + std::to_string(actorList[i]->GetY()));
	}
}