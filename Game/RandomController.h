// RandomController.h
// Chris Bowers 9/11/2014

#ifndef __RANDOMCONTROLLER_H__
#define __RANDOMCONTROLLER_H__

#include "ITickable.h"
#include "Actor.h"
#include "Debug.h"
#include "DynamicArray.h"
#include "FixedGrid.h"
#include "SceneManager.h"
#include <cstdlib>
#include <time.h>

class RandomController : public ITickable{
private:
	DynamicArray<Actor*> actorList;	// list of actors
	FixedGrid* grid;				// grid to operate on
	SceneManager* manager;			// manager to remove nodes from
	Debug* debug;					// pointer to Debug object

public:
	RandomController(FixedGrid* pgrid, SceneManager* pmanager);	// constructor

	~RandomController();				// destructor
	void NewActor(std::string label);	// adds a new actor to the list
	void RemoveActor(SceneNode* obj);	// removes an actor from the list
	void Tick(float dt);				// updates position of actors and checks for collisions
	void ActorList();					// outputs a list of all actors
};

#endif
