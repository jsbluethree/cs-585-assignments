// StateActor.h
// Chris Bowers 10/4/2014

#ifndef __STATEACTOR_H__
#define __STATEACTOR_H__

#include "SceneNode.h"
#include <map>

class StateActor : public SceneNode{
public:
	std::map<std::string, float> initBehavior;
	
	StateActor();
};

#endif
