// StateController.cpp
// Chris Bowers 10/4/2014

#include "StateController.h"

StateController::StateController(StateActor* actor) : actor(actor), stateMachine(actor->initBehavior){	// pass behavior through to stateMachine
	// register controller and actor with SceneManager
	SceneManager::GetInstance()->AddTickable(this);
	SceneManager::GetInstance()->AddSceneNode(actor);
	
	// add states and register dispatchers w/ SceneManager
	stateMachine.stateLookup["idle"] = new TestIdleState;
	SceneManager::GetInstance()->AddTickable(&stateMachine.stateLookup["idle"]->events);
	
	stateMachine.stateLookup["moveto"] = new TestMoveToState;
	SceneManager::GetInstance()->AddTickable(&stateMachine.stateLookup["moveto"]->events);
	
	stateMachine.stateLookup["attack"] = new TestAttackState;
	SceneManager::GetInstance()->AddTickable(&stateMachine.stateLookup["attack"]->events);
	
	// set initial state and set up the state transition listener on it
	stateMachine.currentState = stateMachine.stateLookup["idle"];
	stateMachine.stateLookup["idle"]->events.AddListener("state", &stateMachine.stateTransitionListener);
}

StateController::~StateController(){
	delete stateMachine.stateLookup["idle"];
	delete stateMachine.stateLookup["moveto"];
	delete stateMachine.stateLookup["attack"];
}

void StateController::Tick(float dt){
	// in the final product, the list of relevant actors could be dependent on the state, here we just look at all of them
	stateMachine.currentState->Run(actor, stateMachine.behavioralConfig, SceneManager::GetInstance()->NodeList());
}

void TestIdleState::Run(SceneNode* self, std::map<std::string, float> behavioralConfig, DynamicArray<SceneNode*> relevantActors){
	Debug::GetInstance()->Log("Actor in idle state.");
	for (unsigned int i = 0; i < relevantActors.Length(); i++){
		if (relevantActors[i] == self)
			continue;
		if (abs(relevantActors[i]->GetX() - self->GetX()) <= behavioralConfig["MELEE_RANGE"] && abs(relevantActors[i]->GetY() - self->GetY()) <= behavioralConfig["MELEE_RANGE"]) {
			events.AddEvent(new StateEvent("attack"));
			return;
		}
		else if (abs(relevantActors[i]->GetX() - self->GetX()) <= behavioralConfig["SIGHT_RANGE"] && abs(relevantActors[i]->GetY() - self->GetY()) <= behavioralConfig["SIGHT_RANGE"]){
			events.AddEvent(new StateEvent("moveto"));
			return;
		}
	}
}

void TestMoveToState::Run(SceneNode* self, std::map<std::string, float> behavioralConfig, DynamicArray<SceneNode*> relevantActors){
	Debug::GetInstance()->Log("Actor in move to state.");
	for (unsigned int i = 0; i < relevantActors.Length(); i++){
		if (relevantActors[i] == self)
			continue;
		if (abs(relevantActors[i]->GetX() - self->GetX()) <= behavioralConfig["MELEE_RANGE"] && abs(relevantActors[i]->GetY() - self->GetY()) <= behavioralConfig["MELEE_RANGE"]) {
			events.AddEvent(new StateEvent("attack"));
			return;
		}
		else if (abs(relevantActors[i]->GetX() - self->GetX()) <= behavioralConfig["SIGHT_RANGE"] && abs(relevantActors[i]->GetY() - self->GetY()) <= behavioralConfig["SIGHT_RANGE"]){
			return;
		}
	}
	events.AddEvent(new StateEvent("idle"));
}

void TestAttackState::Run(SceneNode* self, std::map<std::string, float> behavioralConfig, DynamicArray<SceneNode*> relevantActors){
	Debug::GetInstance()->Log("Actor in attack state.");
	for (unsigned int i = 0; i < relevantActors.Length(); i++){
		if (relevantActors[i] == self)
			continue;
		if (abs(relevantActors[i]->GetX() - self->GetX()) <= behavioralConfig["MELEE_RANGE"] && abs(relevantActors[i]->GetY() - self->GetY()) <= behavioralConfig["MELEE_RANGE"]) {
			return;
		}
		else if (abs(relevantActors[i]->GetX() - self->GetX()) <= behavioralConfig["SIGHT_RANGE"] && abs(relevantActors[i]->GetY() - self->GetY()) <= behavioralConfig["SIGHT_RANGE"]){
			events.AddEvent(new StateEvent("moveto"));
			return;
		}
	}
	events.AddEvent(new StateEvent("idle"));
}
