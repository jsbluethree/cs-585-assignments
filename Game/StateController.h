// StateController.h
// Chris Bowers 10/4/2014

#ifndef __STATECONTROLLER_H__
#define __STATECONTROLLER_H__

#include "IState.h"
#include "ITickable.h"
#include "StateActor.h"
#include "StateMachine.h"
#include "SceneManager.h"

class StateController : public ITickable{
private:
	StateActor* actor;
	StateMachine stateMachine;

public:
	StateController(StateActor* actor);
	~StateController();

	void Tick(float dt);
};

class TestIdleState : public IState{
public:
	void Run(SceneNode* self, std::map<std::string, float> behavioralConfig, DynamicArray<SceneNode*> relevantActors);
};

class TestMoveToState : public IState{
public:
	void Run(SceneNode* self, std::map<std::string, float> behavioralConfig, DynamicArray<SceneNode*> relevantActors);
};

class TestAttackState : public IState{
public:
	void Run(SceneNode* self, std::map<std::string, float> behavioralConfig, DynamicArray<SceneNode*> relevantActors);
};

#endif
