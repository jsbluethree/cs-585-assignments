// simulation.cpp
// Chris Bowers 9/10/2014

#include "Debug.h"
#include "FixedGrid.h"
#include "SceneManager.h"
#include "Actor.h"
#include "ActorSpawner.h"
#include "RandomController.h"
#include <time.h>

int main()
{
	FixedGrid gameGrid;
	SceneManager* gameManager = SceneManager::GetInstance();
	RandomController gameController(&gameGrid, gameManager);
	ActorSpawner spawner("names.txt", &gameGrid, &gameController, gameManager);

	clock_t startClock = clock();
	
	while (true){
		gameManager->Tick((float)(clock() - startClock) / CLOCKS_PER_SEC);
		if ((clock() - startClock) >= 30 * CLOCKS_PER_SEC)
			break;
	}

	gameController.ActorList();

	return 0;
}

