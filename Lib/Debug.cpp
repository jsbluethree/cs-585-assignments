// Debug.cpp
// Chris Bowers 9/8/2014

#include "Debug.h"

Debug* Debug::instance = nullptr;

Debug::Debug(){
	flags = (D_ERROR | D_COUT_ON);
}

Debug::~Debug(){
	if (outfile.is_open())
		outfile.close();
}

Debug& Debug::GetInstance(){
	if (!instance)
		instance = new Debug;
	return *instance;
}

#ifdef DEBUG_ON

bool Debug::OpenLogFile(std::string filepath){
	if (outfile.is_open())
		outfile.close();
	outfile.open(filepath);
	return outfile.is_open();
}

bool Debug::CloseLogFile(){
	if (outfile.is_open())
		outfile.close();
	return !outfile.is_open();
}

void Debug::Log(char channel, char* message){
	switch (flags & channel) {
	case D_ERROR:
		if ((flags & D_COUT_ON) == D_COUT_ON)
			std::cout << "[ERROR] " << message << std::endl;
		if (outfile.is_open())
			outfile << "[ERROR] " << message << std::endl;
		break;
	case D_GAMEPLAY:
		if ((flags & D_COUT_ON) == D_COUT_ON)
			std::cout << "[GAMEPLAY] " << message << std::endl;
		if (outfile.is_open())
			outfile << "[GAMEPLAY] " << message << std::endl;
		break;
	case D_RENDERING:
		if ((flags & D_COUT_ON) == D_COUT_ON)
			std::cout << "[RENDERING] " << message << std::endl;
		if (outfile.is_open())
			outfile << "[RENDERING] " << message << std::endl;
		break;
	case D_STATEMACHINE:
		if ((flags & D_COUT_ON) == D_COUT_ON)
			std::cout << "[STATEMACHINE] " << message << std::endl;
		if (outfile.is_open())
			outfile << "[STATEMACHINE] " << message << std::endl;
		break;
	case D_EVENTS:
		if ((flags & D_COUT_ON) == D_COUT_ON)
			std::cout << "[EVENTS] " << message << std::endl;
		if (outfile.is_open())
			outfile << "[EVENTS] " << message << std::endl;
		break;
	case D_INPUT:
		if ((flags & D_COUT_ON) == D_COUT_ON)
			std::cout << "[INPUT] " << message << std::endl;
		if (outfile.is_open())
			outfile << "[INPUT] " << message << std::endl;
		break;
	case D_FILES:
		if ((flags & D_COUT_ON) == D_COUT_ON)
			std::cout << "[FILES] " << message << std::endl;
		if (outfile.is_open())
			outfile << "[FILES] " << message << std::endl;
		break;
	}
}

void Debug::Log(char channel, std::string message){
	switch (flags & channel) {
	case D_ERROR:
		if ((flags & D_COUT_ON) == D_COUT_ON)
			std::cout << "[ERROR] " << message << std::endl;
		if (outfile.is_open())
			outfile << "[ERROR] " << message << std::endl;
		break;
	case D_GAMEPLAY:
		if ((flags & D_COUT_ON) == D_COUT_ON)
			std::cout << "[GAMEPLAY] " << message << std::endl;
		if (outfile.is_open())
			outfile << "[GAMEPLAY] " << message << std::endl;
		break;
	case D_RENDERING:
		if ((flags & D_COUT_ON) == D_COUT_ON)
			std::cout << "[RENDERING] " << message << std::endl;
		if (outfile.is_open())
			outfile << "[RENDERING] " << message << std::endl;
		break;
	case D_STATEMACHINE:
		if ((flags & D_COUT_ON) == D_COUT_ON)
			std::cout << "[STATEMACHINE] " << message << std::endl;
		if (outfile.is_open())
			outfile << "[STATEMACHINE] " << message << std::endl;
		break;
	case D_EVENTS:
		if ((flags & D_COUT_ON) == D_COUT_ON)
			std::cout << "[EVENTS] " << message << std::endl;
		if (outfile.is_open())
			outfile << "[EVENTS] " << message << std::endl;
		break;
	case D_INPUT:
		if ((flags & D_COUT_ON) == D_COUT_ON)
			std::cout << "[INPUT] " << message << std::endl;
		if (outfile.is_open())
			outfile << "[INPUT] " << message << std::endl;
		break;
	case D_FILES:
		if ((flags & D_COUT_ON) == D_COUT_ON)
			std::cout << "[FILES] " << message << std::endl;
		if (outfile.is_open())
			outfile << "[FILES] " << message << std::endl;
		break;
	}
}

void Debug::FlagOn(char flag) { flags |= flag; }

void Debug::FlagOff(char flag) { flags &= ~flag; }

#else

bool Debug::OpenLogFile(std::string filepath) { return false; }

bool Debug::CloseLogFile() { return true; }

void Debug::Log(char channel, char* message) {}

void Debug::Log(char channel, std::string message) {}

void Debug::FlagOn(char flag) {}

void Debug::FlagOff(char flag) {}

#endif
