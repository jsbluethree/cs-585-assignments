// Debug.h
// Chris Bowers 9/8/2014

#ifndef __DEBUG_H__
#define __DEBUG_H__

#include <string>
#include <iostream>
#include <fstream>

#define D_ERROR 0x01
#define D_GAMEPLAY 0x02
#define D_RENDERING 0x04
#define D_STATEMACHINE 0x08
#define D_EVENTS 0x10
#define D_INPUT 0x20
#define D_FILES 0x40
#define D_COUT_ON 0x80

// NOTE: if DEBUG_ON is not defined, all functions will do nothing

class Debug{
private:
	static Debug* instance;	// pointer to itself
	std::ofstream outfile;	// file to be printed to
	Debug();				// constructor
	char flags;				// stores the channel flags
public:
	static Debug& GetInstance();			// creates the object if needed and returns it
	void Log(char channel, char* message);	// outputs a message to the specified channel
	void Log(char channel, std::string message);
	
	~Debug();								// destructor
	bool OpenLogFile(std::string filepath);	// opens an output file
	bool CloseLogFile();					// closes the output file
	void FlagOn(char flag);					// turns on the specified channel flag
	void FlagOff(char flag);				// turns off the specified channel flag
};

#endif