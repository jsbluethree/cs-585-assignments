// DynamicArray.h
// Chris Bowers 8/28/2014

#ifndef __DYNAMICARRAY_H__
#define __DYNAMICARRAY_H__

#include <assert.h>
#include <utility>

template <class T> class DynamicArray {
private:
	T* array;				// the array of items
	unsigned int length;	// number of items currently in the array
	unsigned int capacity;	// number of allocated memory positions

public:
	// constructor
	DynamicArray(unsigned int size = 10);

	// copy constructor
	DynamicArray(const DynamicArray<T>& obj);

	// assignment operator
	DynamicArray<T>& operator=(const DynamicArray<T>& obj);

	// move constructor
	DynamicArray(DynamicArray<T>&& obj);

	// move assignment operator
	DynamicArray<T>& operator=(DynamicArray<T>&& obj);

	// destructor
	~DynamicArray();

	// informative functions
	bool IsEmpty();
	unsigned int Length() const;
	unsigned int Capacity();
	unsigned int MemorySize();

	// add item to end
	void Append(const T& item);
	
	// appends each item in the array
	void Append(const DynamicArray<T>& obj);

	// add item to front
	void PushFront(const T& item);

	// swap two items
	void Swap(unsigned int index1, unsigned int index2);

	// insert at an index
	void Insert(unsigned int index, const T& item);

	// retrieve from an index
	T& operator[](unsigned int index) const;

	// also retrieve from an index, non-operator form
	T& At(unsigned int index);

	// remove at an index
	void Delete(unsigned int index);

	// resize when items exceed capacity
	void Resize();

	// sets array to empty state
	void Initialize();
};

template <class T> DynamicArray<T>::DynamicArray(unsigned int size) : array(new T[size]), length(0), capacity(size) {}

template <class T> DynamicArray<T>::DynamicArray(const DynamicArray<T>& obj) : array(new T[obj.capacity]), length(obj.length), capacity(obj.capacity) {
	for (unsigned int i = 0; i < obj.length; i++)
		array[i] = obj.array[i];
}

template <class T> DynamicArray<T>& DynamicArray<T>::operator=(const DynamicArray<T>& obj){
	if (this != &obj){
		delete[] array;
		array = new T[obj.capacity];
		for (unsigned int i = 0; i < obj.length; i++)
			array[i] = obj.array[i];
		capacity = obj.capacity;
		length = obj.length;
	}
	return *this;
}

template <class T> DynamicArray<T>::DynamicArray(DynamicArray<T>&& obj) : array(obj.array), length(obj.length), capacity(obj.capacity) { obj.array = nullptr; }

template <class T> DynamicArray<T>& DynamicArray<T>::operator=(DynamicArray<T>&& obj){
	if (this != &obj){
		delete[] array;
		array = obj.array;
		length = obj.length;
		capacity = obj.capacity;
		obj.array = nullptr;
	}
	return *this;
}

template <class T> DynamicArray<T>::~DynamicArray() { delete[] array; }

template <class T> bool DynamicArray<T>::IsEmpty() { return length == 0; }

template <class T> unsigned int DynamicArray<T>::Length() const { return length; }

template <class T> unsigned int DynamicArray<T>::Capacity() { return capacity; }

template <class T> unsigned int DynamicArray<T>::MemorySize() { return sizeof(T) * capacity; }

template <class T> void DynamicArray<T>::Append(const T& item){
	// if array is at capacity, resize it 
	if (length == capacity)
		Resize();
	// adds item to the end and increments length
	array[length++] = item;
}

template <class T> void DynamicArray<T>::Append(const DynamicArray<T>& obj){
	for (unsigned int i = 0; i < obj.Length(); i++)
		Append(obj[i]);
}

template <class T> void DynamicArray<T>::PushFront(const T& item){
	// if array is at capacity, resize it
	if (length == capacity)
		Resize();
	// starting at the new end, shift all elements up one
	for (unsigned int i = length; i > 0; i--)
		array[i] = std::move(array[i - 1]);
	array[0] = item;
	length++;
}

template <class T> void DynamicArray<T>::Swap(unsigned int index1, unsigned int index2){
	assert(index1 < length);
	assert(index2 < length);
	T placeholder = std::move(array[index1]);
	array[index1] = std::move(array[index2]);
	array[index2] = std::move(placeholder);
}

template <class T> void DynamicArray<T>::Insert(unsigned int index, const T& item){
	assert(index < length);
	// if array is at capacity, resize it
	if (length == capacity)
		Resize();
	// shift all items from the index on up one
	for (unsigned int i = length; i > index; i--)
		array[i] = std::move(array[i - 1]);
	array[index] = item;
	length++;
}

template <class T> T& DynamicArray<T>::operator[](unsigned int index) const {
	assert(index < length);
	return array[index];
}

template <class T> T& DynamicArray<T>::At(unsigned int index){ return operator[](index); }

template <class T> void DynamicArray<T>::Delete(unsigned int index){
	assert(index < length);
	// shift all items past the index down one
	for (unsigned int i = index; i < length - 1; i++)
		array[i] = std::move(array[i + 1]);
	length--;
}

template <class T> void DynamicArray<T>::Resize(){
	// make a new array, fill it with items from the old array
	unsigned int size = (unsigned int)(capacity * 1.5);

	T* newArray = new T[size];
	unsigned int newLength = length;
	// check if we are resizing smaller than the current length
	if (length > size)
		newLength = size;
	for (unsigned int i = 0; i < newLength; i++)
		newArray[i] = std::move(array[i]);
	// cleanup
	delete[] array;
	array = newArray;
	length = newLength;
	capacity = size;
}

template <class T> void DynamicArray<T>::Initialize(){ length = 0; }

#endif
