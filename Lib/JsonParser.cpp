// JsonParser.cpp
// Chris Bowers 9/16/2014

#include "JsonParser.h"

JsonParser* JsonParser::instance = nullptr;

JsonParser::JsonParser(){};

JsonParser& JsonParser::GetInstance(){
	if (!instance)
		instance = new JsonParser;
	return *instance;
}

JsonValue::~JsonValue() {}

JsonArray::~JsonArray(){
	for (unsigned int i = 0; i < array.Length(); i++)
		delete array[i];
}

JsonObject::~JsonObject(){
	for (unsigned int i = 0; i < keyList.Length(); i++)
		delete trie[keyList[i]];
}

void JsonArray::Print(std::ostream& out){
	out << "[ ";
	for (unsigned int i = 0; i < array.Length(); i++){
		array[i]->Print(out);
		if (i != array.Length() - 1)
			out << ", ";
	}
	out << " ]";
}

void JsonObject::Print(std::ostream& out){
	out << "{\n";
	for (unsigned int i = 0; i < keyList.Length(); i++){
		out << '"' << keyList[i] << "\" : ";
		trie[keyList[i]]->Print(out);
		if (i != keyList.Length() - 1)
			out << ",\n";
	}
	out << "\n}";
}

void JsonString::Print(std::ostream& out){ out << '"' << string << '"'; }

void JsonNumber::Print(std::ostream& out){ out << number; }

void JsonNull::Print(std::ostream& out){ out << "null"; }

void JsonBoolean::Print(std::ostream& out){
	if (boolean)
		out << "true";
	else
		out << "false";
}

JsonValue* JsonParser::Parse(std::string filepath){
	std::ifstream infile;
	infile.open(filepath);
	JsonValue* topItem = nullptr;
	while (isspace(infile.peek()))
		infile.ignore();
	if (infile.eof())
		return new JsonNull;
	else if (infile.peek() == '{')
		topItem = new JsonObject;	
	else if (infile.peek() == '[')
		topItem = new JsonArray;
	else if (infile.peek() == '"')
		topItem = new JsonString;
	else if (infile.peek() == 'f' || infile.peek() == 't')
		topItem = new JsonBoolean;
	else if (infile.peek() == 'n')
		topItem = new JsonNull;
	else
		topItem = new JsonNumber;
	topItem->Populate(infile);
	return topItem;
}

void JsonObject::Populate(std::istream& input){
	assert(input.get() == '{');
	while (true){
		while (isspace(input.peek()))
			input.ignore();
		if (input.peek() == '}'){
			input.ignore();
			break;
		}
		else if (input.peek() == ',')
			input.ignore();
		while (isspace(input.peek()))
			input.ignore();

		// first get the string part
		assert(input.get() == '"');
		std::string key = "";
		while (true){
			char currentChar = input.get();
			if (currentChar == '"')
				break;
			else if (currentChar == '\\'){
				if (input.peek() == '"'){
					input.ignore();
					key = key + "\"";
				}
				else if (input.peek() == '\\'){
					input.ignore();
					key = key + "\\";
				}
				else if (input.peek() == '/'){
					input.ignore();
					key = key + "\\/";
				}
				else if (input.peek() == 'b'){
					input.ignore();
					key = key + "\b";
				}
				else if (input.peek() == 'f'){
					input.ignore();
					key = key + "\f";
				}
				else if (input.peek() == 'n'){
					input.ignore();
					key = key + "\n";
				}
				else if (input.peek() == 'r'){
					input.ignore();
					key = key + "\r";
				}
				else if (input.peek() == 't'){
					input.ignore();
					key = key + "\t";
				}
			}
			else {
				key = key + currentChar;
			}
		}
		while (isspace(input.peek()))
			input.ignore();
		assert(input.get() == ':');

		// then get the value
		while (isspace(input.peek()))
			input.ignore();
		JsonValue* newItem = nullptr;
		if (input.peek() == '{')
			newItem = new JsonObject;
		else if (input.peek() == '[')
			newItem = new JsonArray;
		else if (input.peek() == '"')
			newItem = new JsonString;
		else if (input.peek() == 'f' || input.peek() == 't')
			newItem = new JsonBoolean;
		else if (input.peek() == 'n')
			newItem = new JsonNull;
		else
			newItem = new JsonNumber;
		newItem->Populate(input);
		keyList.Append(key);
		trie.Add(key, newItem);
	}	
}

void JsonArray::Populate(std::istream& input){
	assert(input.get() == '[');
	while (true){
		while (isspace(input.peek()))
			input.ignore();
		if (input.peek() == ']'){
			input.ignore();
			break;
		}
		else if (input.peek() == ',')
			input.ignore();
		while (isspace(input.peek()))
			input.ignore();
		JsonValue* newItem = nullptr;
		if (input.peek() == '{')
			newItem = new JsonObject;
		else if (input.peek() == '[')
			newItem = new JsonArray;
		else if (input.peek() == '"')
			newItem = new JsonString;
		else if (input.peek() == 'f' || input.peek() == 't')
			newItem = new JsonBoolean;
		else if (input.peek() == 'n')
			newItem = new JsonNull;
		else
			newItem = new JsonNumber;
		newItem->Populate(input);
		array.Append(newItem);
		while (isspace(input.peek()))
			input.ignore();
		
	}
	
}

void JsonString::Populate(std::istream& input){
	assert(input.get() == '"');
	string = "";
	while (true){
		char currentChar = input.get();
		if (currentChar == '"')
			break;
		else if (currentChar == '\\'){
			if (input.peek() == '"'){
				input.ignore();
				string = string + "\"";
			}
			else if (input.peek() == '\\'){
				input.ignore();
				string = string + "\\";
			}
			else if (input.peek() == '/'){
				input.ignore();
				string = string + "\\/";
			}
			else if (input.peek() == 'b'){
				input.ignore();
				string = string + "\b";
			}
			else if (input.peek() == 'f'){
				input.ignore();
				string = string + "\f";
			}
			else if (input.peek() == 'n'){
				input.ignore();
				string = string + "\n";
			}
			else if (input.peek() == 'r'){
				input.ignore();
				string = string + "\r";
			}
			else if (input.peek() == 't'){
				input.ignore();
				string = string + "\t";
			}
		}
		else {
			string = string + currentChar;
		}
	}	
}

void JsonBoolean::Populate(std::istream& input){
	if (input.peek() == 't'){
		assert(input.get() == 't');
		assert(input.get() == 'r');
		assert(input.get() == 'u');
		assert(input.get() == 'e');
		boolean = true;
	}
	else {
		assert(input.get() == 'f');
		assert(input.get() == 'a');
		assert(input.get() == 'l');
		assert(input.get() == 's');
		assert(input.get() == 'e');
		boolean = false;
	}	
}

void JsonNull::Populate(std::istream& input){
	assert(input.get() == 'n');
	assert(input.get() == 'u');
	assert(input.get() == 'l');
	assert(input.get() == 'l');
}

void JsonNumber::Populate(std::istream& input){ input >> number; }