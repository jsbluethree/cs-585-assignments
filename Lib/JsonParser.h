// JsonParser.h
// Chris Bowers 9/16/2014

#ifndef __JSONPARSER_H__
#define __JSONPARSER_H__

#include "DynamicArray.h"
#include "WideTrie.h"
#include <fstream>
#include <iostream>
#include <string>

class JsonValue{
public:
	virtual void Print(std::ostream& out = std::cout) = 0;	// outputs the value to the given stream
	virtual void Populate(std::istream& input) = 0;			// populates the value from the given stream
	
	virtual ~JsonValue();	// virtual destructor
};

class JsonParser{
private:
	static JsonParser* instance;	// pointer to itself
	JsonParser();					// constructor (empty)
public:
	static JsonParser& GetInstance();		// returns pointer to self
	JsonValue* Parse(std::string filepath);	// returns a pointer to the object described in the given file
};

class JsonArray : public JsonValue{
public:
	DynamicArray<JsonValue*> array;	// array of pointers to values

	~JsonArray();	// destructor

	void Print(std::ostream& out = std::cout);	// outputs the value to the given stream
	void Populate(std::istream& input);			// populates the value from the given stream
};

class JsonObject : public JsonValue{
public:
	WideTrie<JsonValue*> trie;			// trie of values with associated keys
	DynamicArray<std::string> keyList;	// list of keys kept for quick retrieval

	~JsonObject();	// destructor

	void Print(std::ostream& out = std::cout);	// outputs the value to the given stream
	void Populate(std::istream& input);			// populates the value from the given stream
};

class JsonString : public JsonValue{
public:
	std::string string;

	void Print(std::ostream& out = std::cout);	// outputs the value to the given stream
	void Populate(std::istream& input);			// populates the value from the given stream
};

class JsonBoolean : public JsonValue{
public:
	bool boolean;

	void Print(std::ostream& out = std::cout);	// outputs the value to the given stream
	void Populate(std::istream& input);			// populates the value from the given stream
};

class JsonNull : public JsonValue{
public:
	void Print(std::ostream& out = std::cout);	// outputs the value to the given stream
	void Populate(std::istream& input);			// populates the value from the given stream
};

class JsonNumber : public JsonValue{
public:
	double number;

	void Print(std::ostream& out = std::cout);	// outputs the value to the given stream
	void Populate(std::istream& input);			// populates the value from the given stream
};
#endif
