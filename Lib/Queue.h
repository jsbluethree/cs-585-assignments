// Queue.h
// Chris Bowers 9/3/2014

#ifndef __QUEUE_H__
#define __QUEUE_H__

#include <assert.h>

template <class T> class Queue{
private:
	T* array;		// the array of items - implemented as a circular array
	unsigned int front;		// the index of the front of the queue
	unsigned int rear;		// the index of the rear of the queue
	unsigned int capacity;	// number of allocated memory positions

public:
	// constructor
	Queue(unsigned int size = 10);

	// copy constructor
	Queue(const Queue<T>& obj);

	// assignment operator
	Queue<T>& operator=(const Queue<T>& obj);

	// move constructor
	Queue(Queue<T>&& obj);

	// move assignment operator
	Queue<T>& operator=(Queue<T>&& obj);

	// destructor
	~Queue();

	// informative functions
	bool IsEmpty();
	bool IsFull();
	unsigned int Length();
	unsigned int Capacity();
	unsigned int MemorySize();

	// returns front element of the queue
	T& Front();

	// adds item to rear of queue
	void EnQueue(const T& newItem);

	// removes front element of queue
	void DeQueue();

	// resize when items exceed capacity
	void Resize();
};

template <class T> Queue<T>::Queue(unsigned int size) : array(new T[size]), capacity(size), front(0), rear(size - 1) {}

template <class T> Queue<T>::Queue(const Queue<T>& obj) : array(new T[obj.capacity]), capacity(obj.capacity), front(obj.front), rear(obj.rear) {
	for (unsigned int i = obj.front; i % obj.capacity != obj.rear + 1; i++)
		array[i % obj.capacity] = obj.array[i % obj.capacity];
}

template <class T> Queue<T>& Queue<T>::operator=(const Queue<T>& obj){
	if (this != &obj){
		delete[] array;
		array = new T[obj.capacity];
		for (unsigned int i = obj.front; i % obj.capacity != obj.rear + 1; i++)
			array[i % obj.capacity] = obj.array[i % obj.capacity];
		capacity = obj.capacity;
		front = obj.front;
		rear = obj.rear;
	}
	return *this;
}

template <class T> Queue<T>::Queue(Queue<T>&& obj) : array(obj.array), capacity(obj.capacity), front(obj.front), rear(obj.rear) { obj.array = nullptr; }

template <class T> Queue<T>& Queue<T>::operator=(Queue<T>&& obj){
	if (this != &obj){
		delete[] array;
		array = obj.array;
		capacity = obj.capacity;
		front = obj.front;
		rear = obj.rear;
		obj.array = nullptr;
	}
	return *this;
}

template <class T> Queue<T>::~Queue() { delete[] array; }

template <class T> bool Queue<T>::IsEmpty() { return front == (rear + 1) % capacity; }

template <class T> bool Queue<T>::IsFull() { return front == (rear + 2) % capacity; }

template <class T> unsigned int Queue<T>::Length(){
	if (IsEmpty())
		return 0;
	else if (rear >= front)
		return rear - front + 1;
	else
		return rear - front + capacity + 1;
}

template <class T> unsigned int Queue<T>::Capacity() { return capacity; }

template <class T> unsigned int Queue<T>::MemorySize() { return sizeof(T) * capacity; }

template <class T> T& Queue<T>::Front(){
	assert(!IsEmpty());
	return array[front];
}

template <class T> void Queue<T>::EnQueue(const T& newItem){
	if (IsFull())
		Resize();
	rear = (rear + 1) % capacity;
	array[rear] = newItem;
}

template <class T> void Queue<T>::DeQueue(){
	if (!IsEmpty());
	front = (front + 1) % capacity;
}

template <class T> void Queue<T>::Resize(){
	int size = capacity * 1.5;
	T* newArray = new T[size];
	int newLength = Length();
	if (newLength >= size)
		newLength = size - 1;
	for (unsigned int i = front; i < front + newLength; i++)
		newArray[i % size] = array[i % capacity];
	delete[] array;
	array = newArray;
	rear = (front + newLength - 1) % size;
	capacity = size;
}

#endif
