// Stack.h 
// Chris Bowers 9/2/2014

#ifndef __STACK_H__
#define __STACK_H__

#include "DynamicArray.h"

template <class T> class Stack{
private:
	DynamicArray<T> container;

public:
	T& Top() { return container[Length() - 1]; }
	void Push(T& newItem){ container.Append(newItem); }
	void Pop() { container.Delete(Length() - 1); }

	bool IsEmpty() { return container.IsEmpty(); }
	int Length() { return container.Length(); }
	int Capacity() { return container.Capacity(); }
	int MemorySize() { return container.MemorySize(); }
};

#endif
