// Trie.h
// Chris Bowers 9/8/2014

#ifndef __TRIE_H__
#define __TRIE_H__

#include <string>
#include <assert.h>
#include <iostream>
#include "Debug.h"

template <class T> class Trie{
private:
	class TrieNode{
	public:
		T data;
		bool wordEnd;
		TrieNode* array[26];

		TrieNode();		// constructor
		~TrieNode();	// destructor

		void Copy(TrieNode* original);	// creates child nodes equivalent to original
	};

	TrieNode* root;

public:
	
	Trie();		// constructor
	~Trie();	// destructor	

	Trie(const Trie<T>& obj);				// copy constructor	
	Trie& operator=(const Trie<T>& obj);	// assignment operator	
	Trie(Trie<T>&& obj);					// move constructor	
	Trie& operator=(Trie<T>&& obj);			// move assignment operator

	TrieNode* Root();	// returns pointer to root node

	void Add(std::string word, const T& item);	// add an item with an associated hash string	
	bool Search(std::string word);				// determine whether a word is in the trie	
	T& operator[](std::string word);			// returns the data in a node given the associated word, will construct new object if none exists	
	T& At(std::string word);					// same functionality as operator[] but will not construct new object, instead causing an error
	void Delete(std::string word);				// removes a key from the trie

	// outputs each key in order to standard output, pass Root() to get all keys
	friend void SortKeys(TrieNode* start, char separator = '\n', std::string outString = ""){
		if (start->wordEnd)
			std::cout << outString << separator;
		for (unsigned int i = 0; i < 26; i++){
			if (start->array[i]){
				char currentChar = i + 'a';
				std::string partString = outString + currentChar;
				SortKeys(start->array[i], separator, partString);
			}
		}
	}
};

template <class T> Trie<T>::TrieNode::TrieNode(){
	for (int i = 0; i < 26; i++)
		array[i] = nullptr;
	wordEnd = false;
}

template <class T> Trie<T>::TrieNode::~TrieNode(){
	for (int i = 0; i < 26; i++)
		delete array[i];
}

template <class T> void Trie<T>::TrieNode::Copy(TrieNode* original){
	data = original->data;
	for (int i = 0; i < 26; i++){
		delete array[i];
		if (original->array[i]){
			array[i] = new TrieNode;
			array[i]->Copy(original->array[i]);
		}
		else
			array[i] = nullptr;
	}
}

template <class T> Trie<T>::Trie() : root(new Trie<T>::TrieNode()) {}

template <class T> Trie<T>::~Trie(){ delete root; }

template <class T> Trie<T>::Trie(const Trie<T>& obj) : root(new Trie<T>::TrieNode()) { root->Copy(obj.root); }

template <class T> Trie<T>& Trie<T>::operator=(const Trie<T>& obj){
	if (this != &obj){
		root->Copy(obj.root);
	}
	return *this;
}

template <class T> Trie<T>::Trie(Trie<T>&& obj) : root(obj.root) { obj.root = nullptr; }

template <class T> Trie<T>& Trie<T>::operator=(Trie<T>&& obj){
	if (this != &obj){
		delete root;
		root = obj.root;
		obj.root = nullptr;
	}
	return *this;
}

template <class T> typename Trie<T>::TrieNode* Trie<T>::Root() { return root; }

template <class T> void Trie<T>::Add(std::string word, const T& item){
	TrieNode* currentNode = root;
	for (unsigned int i = 0; i < word.length(); i++){
		char currentChar = tolower(word[i]);
		int index = currentChar - 'a';
		assert(index >= 0 && index <= 25);
		if (currentNode->array[index])
			currentNode = currentNode->array[index];
		else{
			currentNode->array[index] = new TrieNode;
			currentNode = currentNode->array[index];
		}
	}
	currentNode->wordEnd = true;
	currentNode->data = item;
}

template <class T> bool Trie<T>::Search(std::string word){
	TrieNode* currentNode = root;
	for (unsigned int i = 0; i < word.length(); i++){
		char currentChar = tolower(word[i]);
		int index = currentChar - 'a';
		assert(index >= 0 && index <= 25);
		if (currentNode->array[index])
			currentNode = currentNode->array[index];
		else
			return false;
		if (i == word.length() - 1 && !currentNode->wordEnd)
			return false;
	}
	return true;
}

template <class T> T& Trie<T>::operator[](std::string word){
	TrieNode* currentNode = root;
	for (unsigned int i = 0; i < word.length(); i++){
		char currentChar = tolower(word[i]);
		int index = currentChar - 'a';
		assert(index >= 0 && index <= 25);
		if (currentNode->array[index])
			currentNode = currentNode->array[index];
		else{
			currentNode->array[index] = new TrieNode;
			currentNode = currentNode->array[index];
		}
	}
	currentNode->wordEnd = true;
	return currentNode->data;
}

template <class T> T& Trie<T>::At(std::string word){
	TrieNode* currentNode = root;
	for (unsigned int i = 0; i < word.length(); i++){
		char currentChar = tolower(word[i]);
		int index = currentChar - 'a';
		assert(index >= 0 && index <= 25);
		if (currentNode->array[index])
			currentNode = currentNode->array[index];
		else{
			assert(Search(word));
			return root->data;
			//error
		}
		if (i == word.length() - 1){
			if (currentNode->wordEnd)
				return currentNode->data;
			else{
				assert(Search(word));
				return root->data;
				//error
			}
		}
	}
	// this line should never be reached
	assert(Search(word));
	return root->data;
}

template <class T> void Trie<T>::Delete(std::string word){
	TrieNode* currentNode = root;
	for (unsigned int i = 0; i < word.length(); i++){
		char currentChar = tolower(word[i]);
		int index = currentChar - 'a';
		assert(index >= 0 && index <= 25);
		if (currentNode->array[index]){
			currentNode = currentNode->array[index];
		}
		else
			return;
		if (i == word.length() - 1 && currentNode->wordEnd){
			currentNode->wordEnd = false;
		}
	}
}

#endif
