// WideTrie.h
// Chris Bowers 9/8/2014

#ifndef __WIDETRIE_H__
#define __WIDETRIE_H__

#include <string>
#include <assert.h>
#include <iostream>
#include "Debug.h"

template <class T> class WideTrie{
private:
	class WideNode{
	public:
		T data;
		bool wordEnd;
		WideNode* array[256];

		WideNode();		// constructor
		~WideNode();	// destructor

		void Copy(WideNode* original);	// creates child nodes equivalent to original
	};

	WideNode* root;

public:
	
	WideTrie();		// constructor
	~WideTrie();	// destructor	

	WideTrie(const WideTrie<T>& obj);				// copy constructor	
	WideTrie& operator=(const WideTrie<T>& obj);	// assignment operator	
	WideTrie(WideTrie<T>&& obj);					// move constructor	
	WideTrie& operator=(WideTrie<T>&& obj);			// move assignment operator

	WideNode* Root();	// returns pointer to root node

	void Add(std::string word, const T& item);	// add an item with an associated hash string	
	bool Search(std::string word);				// determine whether a word is in the trie	
	T& operator[](std::string word);			// returns the data in a node given the associated word, will construct new object if none exists
	T& At(std::string word);					// same functionality as operator[] but will not construct new object, instead causing an error	
	void Delete(std::string word);				// removes a key from the trie

	// outputs each key in order to standard output, pass Root() to get all keys
	friend void SortKeys(WideNode* start, char separator = '\n', std::string outString = ""){
		if (start->wordEnd)
			std::cout << outString << separator;
		for (unsigned int i = 0; i < 256; i++){
			if (start->array[i]){
				char currentChar = (char)i;
				std::string partString = outString + currentChar;
				SortKeys(start->array[i], separator, partString);
			}
		}
	}
};

template <class T> WideTrie<T>::WideNode::WideNode(){
	for (int i = 0; i < 256; i++)
		array[i] = nullptr;
	wordEnd = false;
}

template <class T> WideTrie<T>::WideNode::~WideNode(){
	for (int i = 0; i < 256; i++)
		delete array[i];
}

template <class T> void WideTrie<T>::WideNode::Copy(WideNode* original){
	data = original->data;
	for (int i = 0; i < 256; i++){
		delete array[i];
		if (original->array[i]){
			array[i] = new WideNode;
			array[i]->Copy(original->array[i]);
		}
		else
			array[i] = nullptr;
	}
}

template <class T> WideTrie<T>::WideTrie() : root(new WideTrie<T>::WideNode()) {}

template <class T> WideTrie<T>::~WideTrie(){ delete root; }

template <class T> WideTrie<T>::WideTrie(const WideTrie<T>& obj) : root(new WideTrie<T>::WideNode()) { root->Copy(obj.root); }

template <class T> WideTrie<T>& WideTrie<T>::operator=(const WideTrie<T>& obj){
	if (this != &obj){
		root->Copy(obj.root);
	}
	return *this;
}

template <class T> WideTrie<T>::WideTrie(WideTrie<T>&& obj) : root(obj.root) { obj.root = nullptr; }

template <class T> WideTrie<T>& WideTrie<T>::operator=(WideTrie<T>&& obj){
	if (this != &obj){
		delete root;
		root = obj.root;
		obj.root = nullptr;
	}
	return *this;
}

template <class T> typename WideTrie<T>::WideNode* WideTrie<T>::Root() { return root; }

template <class T> void WideTrie<T>::Add(std::string word, const T& item){
	WideNode* currentNode = root;
	for (unsigned int i = 0; i < word.length(); i++){
		int index = (int)word[i];
		if (currentNode->array[index])
			currentNode = currentNode->array[index];
		else{
			currentNode->array[index] = new WideNode;
			currentNode = currentNode->array[index];
		}
	}
	currentNode->wordEnd = true;
	currentNode->data = item;
}

template <class T> bool WideTrie<T>::Search(std::string word){
	WideNode* currentNode = root;
	for (unsigned int i = 0; i < word.length(); i++){
		int index = (int)word[i];
		if (currentNode->array[index])
			currentNode = currentNode->array[index];
		else
			return false;
		if (i == word.length() - 1 && !currentNode->wordEnd)
			return false;
	}
	return true;
}

template <class T> T& WideTrie<T>::operator[](std::string word){
	WideNode* currentNode = root;
	for (unsigned int i = 0; i < word.length(); i++){
		int index = (int)word[i];
		if (currentNode->array[index])
			currentNode = currentNode->array[index];
		else{
			currentNode->array[index] = new WideNode;
			currentNode = currentNode->array[index];
		}
	}
	currentNode->wordEnd = true;
	return currentNode->data;
}

template <class T> T& WideTrie<T>::At(std::string word){
	WideNode* currentNode = root;
	for (unsigned int i = 0; i < word.length(); i++){
		int index = (int)word[i];
		if (currentNode->array[index])
			currentNode = currentNode->array[index];
		else{
			assert(Search(word));
			return root->data;
			//error
		}
		if (i == word.length() - 1){
			if (currentNode->wordEnd)
				return currentNode->data;
			else{
				assert(Search(word));
				return root->data;
				//error
			}
		}
	}
	// this line should never be reached
	assert(Search(word));
	return root->data;
}

template <class T> void WideTrie<T>::Delete(std::string word){
	WideNode* currentNode = root;
	for (unsigned int i = 0; i < word.length(); i++){
		int index = (int)word[i];
		if (currentNode->array[index]){
			currentNode = currentNode->array[index];
		}
		else
			return;
		if (i == word.length() - 1 && currentNode->wordEnd){
			currentNode->wordEnd = false;
		}
	}
}

#endif
