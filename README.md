Coding style:

Each file begins with a comment containing the name of the file, my name, and the date the file was made.

Comments are either in the same line as the code they refer to, or immediately preceding.

Variable names are uncapitalized nouns. Names with multiple words have each word other than the first capitalized with no spaces. ex:

```
#!c++

length, index, placeholder, testArray, waffleHouse
```


Class names are capitalized nouns. Names with multiple words have each word capitalized with no spaces. ex: 

```
#!c++

State, DynamicArray, PriorityQueue, Actor
```


Function and method names are verbs or verblike phrases (prepositions may be omitted) that follow the same capitalization rules as class names. ex:

```
#!c++

Append, PrintList, Update, PushFront
```


An exception is methods that are used to view private members of classes or similar information. These will be capitalized versions of those members or a relevant noun. ex:

```
#!c++

Length, Value, Capacity, MemorySize
```


Boolean variables are adjectives or adjectivelike phrases. ex:

```
#!c++

found, checked, purple, mammalian
```


Boolean functions and methods are adjectives or adjectivelike phrases preceded by 'Is'. ex:

```
#!c++

IsEmpty, IsRed, IsAMammal, IsBestGirl
```


Constants are nouns in all caps, with words separated by underscores. ex:

```
#!c++

FRAMES_PER_SECOND, RED, SCREEN_WIDTH, OLIVE_GARDENS
```


ifndef and define lines in headers are the file name in all caps, with periods replaced by underscores, preceded and followed by two underscores. ex:

```
#!c++

__DYNAMICARRAY_H__, __WAFFLEHOUSE_H__, __BESTGIRL_H__
```


Brackets will be omitted from single line if-blocks or loops if it does not reduce readability. ex:

```
#!c++

for (int i = 0; i < obj.length; i++)
	array[i] = obj.array[i];
```

	
Otherwise open brackets will be on the same line as the if or loop statement, and close brackets will be on their own line. ex:

```
#!c++

for (int i = 0; i < obj.length; i++) {
	array[i] = obj.array[i];
}

```

In for loops, the iterator will be called i unless there is a reason for it to be called something else. In nested for loops, subsequent iterators will be j, k, and so on.