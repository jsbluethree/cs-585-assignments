// CursesGameTest.cpp
// Chris Bowers 10/28/2014

#include "CursesGame.h"

int main(){
	CursesGame game;
	game.ReadLevel("Game/level.json");
	game.Init();	
	game.Run();
	game.Exit();
	return 0;
}
