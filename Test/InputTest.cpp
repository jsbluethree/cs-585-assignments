// InputTest.cpp
// Chris Bowers

#include <ncurses.h>
#include "Input.h"
#include "ICallback.h"

class KeyDownListener : public ICallback{
public:
	bool quit;

	KeyDownListener();
	
	void Execute(IEvent* event);
};

class KeyUpListener : public ICallback{
public:
	void Execute(IEvent* event);
};

KeyDownListener::KeyDownListener() { quit = false; }

void KeyDownListener::Execute(IEvent* event){
	printw("Key %c down\n", static_cast<KeyDownEvent*>(event)->key);
	if (static_cast<KeyDownEvent*>(event)->key == 27)
		quit = true;
}

void KeyUpListener::Execute(IEvent* event){
	printw("Key %c up\n", static_cast<KeyUpEvent*>(event)->key);
}

int main(){
	// initialize ncurses
	initscr();
	// don't buffer inputs
	cbreak();
	// don't echo inputs
	noecho();
	// delay half a second on input
	halfdelay(5);
	// enable keypad inputs
	keypad(stdscr, TRUE);
	
	KeyDownListener downListener;
	KeyUpListener upListener;
	
	Input::GetInstance().events.AddListener("keydown", &downListener);
	Input::GetInstance().events.AddListener("keyup", &upListener);
	
	while(!downListener.quit){
		Input::GetInstance().Tick(0);
		Input::GetInstance().events.Tick(0);
	}
	
	endwin();
	return 0;
}
