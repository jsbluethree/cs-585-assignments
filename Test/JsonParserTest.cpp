// JsonParserTest.cpp
// Chris Bowers 9/16/2014


#include "JsonParser.h"

int main()
{
	// Before running program, put test code in test.json

	// Parse test.json and print to testout1.json
	JsonValue* testObj = JsonParser::GetInstance()->Parse("test.json");
	std::ofstream outfile;
	outfile.open("testout1.json");
	testObj->Print(outfile);
	outfile.close();

	// To make sure we printed valid json, we parse testout1.json into testout2.json
	JsonValue* retestObj = JsonParser::GetInstance()->Parse("testout1.json");
	outfile.open("testout2.json");
	retestObj->Print(outfile);
	outfile.close();

	// cleanup
	delete testObj;
	delete retestObj;
	return 0;
}

