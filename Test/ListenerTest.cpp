// ListenerTest.cpp 
// Chris Bowers 9/23/2014

#include "MonsterController.h"

int main()
{
	Pawn testPawn;
	MonsterController testController(&testPawn);
	assert(testPawn.health == 10);
	for (int i = 0; i < 20; i++){
		std::cout << testPawn.health << '\n';
		SceneManager::GetInstance()->Tick(0);
	}
	assert(testPawn.health == 0);

	return 0;
}

