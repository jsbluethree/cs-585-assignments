// StateMachineTest.cpp
// Chris Bowers 9/30/2014

#include "Dispatcher.h"
#include <iostream>
#include "DynamicArray.h"
#include "StateMachine.h"
#include <map>
#include <time.h>
#include "StateController.h"

int main()
{
	StateActor testActor;
	StateController testController(&testActor);

	clock_t startClock = clock();

	testActor.SetX(0);
	testActor.SetY(0);

	Debug::GetInstance()->Log("TestActor initialized at 0, 0");


	SceneManager::GetInstance()->Tick((float)(clock() - startClock) / CLOCKS_PER_SEC);
	std::cout << '\n';
	SceneNode farNode;
	SceneManager::GetInstance()->AddSceneNode(&farNode);
	Debug::GetInstance()->Log("FarNode created at 20, 20");
	farNode.SetX(20);
	farNode.SetY(20);


	SceneManager::GetInstance()->Tick((float)(clock() - startClock) / CLOCKS_PER_SEC);
	std::cout << '\n';
	SceneNode nearNode;
	SceneManager::GetInstance()->AddSceneNode(&nearNode);
	Debug::GetInstance()->Log("NearNode created at 5, 5");
	nearNode.SetX(5);
	nearNode.SetY(5);


	SceneManager::GetInstance()->Tick((float)(clock() - startClock) / CLOCKS_PER_SEC);
	std::cout << '\n';
	std::cout << '\n';
	SceneManager::GetInstance()->RemoveSceneNode(&nearNode);
	Debug::GetInstance()->Log("NearNode removed from Scene Manager");

	SceneManager::GetInstance()->Tick((float)(clock() - startClock) / CLOCKS_PER_SEC);
	std::cout << '\n';
	SceneManager::GetInstance()->Tick((float)(clock() - startClock) / CLOCKS_PER_SEC);
	std::cout << '\n';
	farNode.SetX(1);
	farNode.SetY(1);

	Debug::GetInstance()->Log("FarNode moved to 1,1");

	SceneManager::GetInstance()->Tick((float)(clock() - startClock) / CLOCKS_PER_SEC);
	std::cout << '\n';
	SceneManager::GetInstance()->Tick((float)(clock() - startClock) / CLOCKS_PER_SEC);
	std::cout << '\n';

	SceneManager::GetInstance()->Tick((float)(clock() - startClock) / CLOCKS_PER_SEC);
	return 0;
}

