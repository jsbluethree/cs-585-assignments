// Trietest.cpp
// Chris Bowers 9/8/2014

#include "Trie.h"
#include <iostream>
#include "Debug.h"
#define DEBUG_ON


int main()
{	
	// test constructor
	Trie<int> testTrie;
	// test copy constructor
	Trie<int> bestTrie(testTrie);
	// test add
	testTrie.Add("fish", 3);
	testTrie.Add("filled", 6);
	testTrie.Add("duck", 7);
	// test [] operator
	assert(testTrie["fish"] == 3);
	// test delete
	testTrie.Delete("duck");
	//testTrie["duck"]; // program exits here due to error
	// test SortKeys
	testTrie.SortKeys(testTrie.Root());
	// test = operator
	Trie<int> worstTrie = testTrie;
	worstTrie.SortKeys(worstTrie.Root());
	return 0;
}

