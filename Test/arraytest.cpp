// arraytest.cpp 
// Chris Bowers 8/28/2014

#include "DynamicArray.h"
#include <iostream>


int main()
{
	// test constructor with default size
	DynamicArray<int> bestArray;
	// test constructor with given size
	DynamicArray<int> testArray(5);
	std::cout << "[Passed] Constructor functioning properly\n";

	// test for emptiness
	assert(testArray.IsEmpty());
	
	// test appending values
	testArray.Append(3);
	testArray.Append(2);
	testArray.Append(1);
	std::cout << "[Passed] Append functioning properly\n";
	
	// test for non-emptiness
	assert(!testArray.IsEmpty());
	std::cout << "[Passed] IsEmpty functioning properly\n";

	// test getting length
	assert(testArray.Length() == 3);
	std::cout << "[Passed] Length functioning properly\n";

	// test getting capacity
	assert(testArray.Capacity() == 5);
	std::cout << "[Passed] Capacity functioning properly\n";

	// test getting mem size
	assert(testArray.MemorySize() == 20);
	std::cout << "[Passed] MemorySize functioning properly\n";

	// test getting elements
	assert(testArray[0] == 3);
	assert(testArray[1] == 2);
	assert(testArray[2] == 1);
	std::cout << "[Passed] [] operator functioning properly\n";

	// test = operator
	bestArray = testArray;
	assert(bestArray[0] == 3);
	assert(bestArray[1] == 2);
	assert(bestArray[2] == 1);
	std::cout << "[Passed] = operator functioning properly\n";
	
	// test copy constructor
	DynamicArray<int> worstArray(testArray);
	assert(worstArray[0] == 3);
	assert(worstArray[1] == 2);
	assert(worstArray[2] == 1);
	std::cout << "[Passed] copy constructor functioning properly\n";

	// test swapping elements
	testArray.Swap(0, 2);
	assert(testArray[0] == 1);
	assert(testArray[2] == 3);
	std::cout << "[Passed] Swap functioning properly\n";

	// test inserting elements
	testArray.Insert(1, 4);
	assert(testArray[0] == 1);
	assert(testArray[1] == 4);
	assert(testArray[2] == 2);
	assert(testArray[3] == 3);
	std::cout << "[Passed] Insert functioning properly\n";

	// test pushing to front
	testArray.PushFront(5);
	assert(testArray[0] == 5);
	assert(testArray[1] == 1);
	assert(testArray[2] == 4);
	assert(testArray[3] == 2);
	assert(testArray[4] == 3);
	std::cout << "[Passed] PushFront functioning properly\n";

	// test delete
	testArray.Delete(0);
	assert(testArray[0] == 1);
	assert(testArray[1] == 4);
	assert(testArray[2] == 2);
	assert(testArray[3] == 3);
	std::cout << "[Passed] Delete functioning properly\n";

	// test resizing
	testArray.Append(5);
	testArray.Append(6);
	assert(testArray.Capacity() == 10);
	std::cout << "[Passed] Resize functioning properly\n";
	std::cout << "\nAll methods of DynamicArray functioning properly.\n";

	return 0;
}