// listtest.cpp
// Chris Bowers 9/2/2014

#include "LinkedList.h"
#include <iostream>

int main()
{
	// test constructor
	LinkedList<int> testList;

	// test emptiness
	assert(testList.IsEmpty());

	// test adding values from both sides
	testList.AddFirst(5);
	testList.AddLast(3);

	// test not emptiness
	assert(!testList.IsEmpty());

	// test copy constructor
	LinkedList<int> bestList(testList);

	// test = operator
	LinkedList<int> worstList = bestList;

	// test iterator construction
	LinkedList<int>::Iterator testIter = testList.Begin();

	// test AddAfter
	testList.AddAfter(testIter, 4);

	// test iterator increment
	++testIter;
	assert(testIter.Show() == 4);
	testList.AddAfter(testIter, 6);

	// test ShowLast
	assert(testList.ShowLast() == 3);

	// test RemoveLast
	testList.RemoveLast();
	assert(testList.ShowLast() == 6);

	// test RemoveAfter
	testList.RemoveAfter(testIter);
	assert(testList.ShowLast() == 4);

	// test ShowFirst
	assert(testList.ShowFirst() == 5);

	// test Length
	assert(testList.Length() == 2);

	// test set to empty
	testList.SetToEmpty();
	assert(testList.Length() == 0);

	return 0;
}

