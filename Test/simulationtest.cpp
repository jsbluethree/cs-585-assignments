// simulationtest.cpp
// Chris Bowers
// unit test for simulation related classes


#include "ActorSpawner.h"


int blank(){
	FixedGrid gameGrid();
	SceneManager* gameManager = SceneManager::GetInstance();
	RandomController gameController(&gameGrid, gameManager);
	ActorSpawner spawner("names.txt", &gameGrid, &gameController, gameManager);


	// all functions should be called by tick.
	for (float i = 0; i < 50; i = i + 0.25){
		gameManager->Tick(i);
	}

	gameController.ActorList();

	return 0;
}