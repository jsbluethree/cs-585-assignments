CC=g++
CFLAGS=-c -std=c++0x -Wall -Werror
IFLAGS=-ILib -IEngine -IGame -ITest

InputTest: InputTest.o Input.o Dispatcher.o IEvent.o
	$(CC) InputTest.o Input.o Dispatcher.o IEvent.o -lncurses -o InputTest
	
InputTest.o: Test/InputTest.cpp
	$(CC) $(CFLAGS) $(IFLAGS) Test/InputTest.cpp
	
Input.o: Engine/Input.cpp
	$(CC) $(CFLAGS) $(IFLAGS) Engine/Input.cpp
	
Dispatcher.o: Engine/Dispatcher.cpp
	$(CC) $(CFLAGS) $(IFLAGS) Engine/Dispatcher.cpp
	
IEvent.o: Engine/IEvent.cpp
	$(CC) $(CFLAGS) $(IFLAGS) Engine/IEvent.cpp	

clean: 
	rm -rf *o InputTest
